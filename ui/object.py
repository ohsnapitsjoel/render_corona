import bpy

#---------------------------------------
# Object proxy settings UI
#---------------------------------------
class CoronaObjectPropsPanel( bpy.types.Panel):
    bl_label = "Corona Object Properties"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer == 'CORONA' and context.active_object is not None and context.active_object.type in ['MESH', 'SURFACE']

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_obj = context.object.corona
        row = layout.row()
        row.prop( crn_obj, "use_mblur")
        if crn_obj.use_mblur:
            row.prop( crn_obj, "mblur_type")

        layout.separator()
        
        layout.label( "Proxy Settings:")
        layout.prop( crn_obj, "is_proxy", text = "Use As Corona Proxy")    
        col = layout.column()
        col.active = crn_obj.is_proxy
        col.prop( crn_obj, "use_external")
        if crn_obj.use_external:
            col.prop( crn_obj, "external_instance_mesh")
            col.prop( crn_obj, "external_mtllib", text = "External .mtl")
        else:
            col.prop_search( crn_obj, "instance_mesh", scene, "objects")

        layout.separator()
        
        layout.label( "Object Overrides:")
        layout.prop( crn_obj, "triangulate", text = "Triangulate")
        layout.prop( crn_obj, "override_exclude", text = "Exclude Object From Material Override")


def register():
    bpy.utils.register_class( CoronaObjectPropsPanel)
    
def unregister():
    bpy.utils.unregister_class( CoronaObjectPropsPanel)
