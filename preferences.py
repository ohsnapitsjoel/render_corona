import bpy
from bpy.props import StringProperty

#------------------------------------
# User preferences UI
#------------------------------------
class CoronaPreferencesPanel( bpy.types.AddonPreferences):
    bl_idname = __package__
    corona_path = StringProperty( name = "Corona Path",
                                       description = "Path to Corona executable directory",
                                       subtype = 'DIR_PATH',
                                       default = "")
    corona_exe = StringProperty( name = "Corona Executable",
                                       description = "Corona executable filename",
                                       subtype = 'NONE',
                                       default = "CoronaStandalone_Release.exe")

    def draw(self, context):
        self.layout.prop( self, "corona_path")
        self.layout.prop( self, "corona_exe")


def register():
    bpy.utils.register_class( CoronaPreferencesPanel)
def unregister():
    bpy.utils.unregister_class( CoronaPreferencesPanel)
