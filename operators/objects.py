import bpy

#---------------------------------
#Operator for adding render passes
class CoronaAddObject( bpy.types.Operator):
    bl_label = "Add Object"
    bl_idname = "corona.add_object"

    material = bpy.props.StringProperty(name="Selected Node")
    instance_mesh = bpy.props.StringProperty(name="Selected Instance")


    def execute(self, context):
        # print("Running big preview")
        mat = context.object.active_material
        crn_mat = mat.corona
        if crn_mat.node_tree != '':
            node_tree = bpy.data.node_groups[ crn_mat.node_tree]
            node = node_tree.nodes[self.material]
            collection = node.includes
            num = collection.__len__()
            collection.add()
            collection[num].name = self.instance_mesh
        return {'FINISHED'}

    def check(self, context):
        return False

    def invoke( self, context, event):
        # print(self.material)
        wm = context.window_manager
        # print("Invoke big preview")
        return wm.invoke_props_dialog(self, width=300)
        # scene = context.scene
        # collection = scene.corona_passes.passes
        # index = scene.corona_passes.pass_index

        # collection.add()
        # num = collection.__len__()
        # collection[num-1].name = "Render Pass " + str( num)

        # return {'FINISHED'}

    def draw(self, context):
        layout = self.layout
        mat = context.object.active_material
        crn_mat = mat.corona
        # layout.label(mat.name)
        # layout.label(self.material)
        # layout.label(crn_mat.node_tree)
        if crn_mat.node_tree != '':
            node_tree = bpy.data.node_groups[ crn_mat.node_tree]
            node = node_tree.nodes[self.material]
            # layout.label(node.name)
            layout.prop_search( self, "instance_mesh", context.scene, "objects")
        # col = layout.column()
        # col.scale_y = 2
        # layout.template_preview( mat, show_buttons=False, preview_id = "corona.big_preview")

#-----------------------------------
#Operator for removing render passes
class CoronaRemoveObject( bpy.types.Operator):
    bl_label = "Remove Object"
    bl_idname = "corona.remove_object"

    material = bpy.props.StringProperty(name="Selected Node")
    index = bpy.props.IntProperty(name="Selected Index")

    def invoke( self, context, event):
        mat = context.object.active_material
        crn_mat = mat.corona
        if crn_mat.node_tree != '':
            node_tree = bpy.data.node_groups[ crn_mat.node_tree]
            node = node_tree.nodes[self.material]
            collection = node.includes
            # num = collection.__len__()
            # collection.add()
            # collection[num].name = self.instance_mesh
            collection.remove(self.index)

        return {'FINISHED'}

def register():
    bpy.utils.register_class( CoronaAddObject)
    bpy.utils.register_class( CoronaRemoveObject)
def unregister():
    bpy.utils.unregister_class( CoronaAddObject)
    bpy.utils.unregister_class( CoronaRemoveObject)
