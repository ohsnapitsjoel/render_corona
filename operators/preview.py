import bpy
from bpy.types import Operator

class BigPreview(Operator):
    bl_idname = "corona.big_preview"
    bl_label = "Big Preview"

    def execute(self, context):
        print("Running big preview")
        return {'FINISHED'}

    def check(self, context):
        return False

    def invoke(self, context, event):
        wm = context.window_manager
        print("Invoke big preview")
        return wm.invoke_props_dialog(self, width=600, height=600)

    def draw(self, context):
        layout = self.layout
        mat = context.object.active_material
        col = layout.column()
        # col.scale_y = 2
        layout.template_preview( mat, show_buttons=False, preview_id = "corona.big_preview")
        # layout.prop(mat.corona, "preview_quality")

def register():
    bpy.utils.register_class(BigPreview)

def unregister():
    bpy.utils.unregister_class(BigPreview)
