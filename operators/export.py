import bpy
import os, time, datetime
import cProfile
from ..util     import *
from ..export   import export
from ..outputs  import *

from xml.etree.ElementTree import Element, Comment, SubElement

EnableProfiling = False
#-----------------------------
# Corona export operator
#-----------------------------
class CoronaExportOperator(bpy.types.Operator):
    '''Export objects only'''
    bl_idname = "corona.export"
    bl_label = "Export"

    _inst_set = set()
    _psysob_set = set()
    _exported_obs = set()
    _exported_meshes = set()

    def execute(self, context):
        scene = context.scene
        start = time.time()

        # If exporting hair enabled, there's a good chance there will actually be particle systems to export.
        # A hair object placeholder is needed before iterating through scene objects. It is not renderable.
        placeholders = []
        result = None

        if scene.corona.export_hair:
            crv = bpy.data.curves.new('corona_hair_tmp_curve', 'CURVE')
            crv_ob = bpy.data.objects.new("%s_ob" % crv.name, crv)
            crv_ob.hide_render = True
            placeholders = [crv, crv_ob]
            if EnableProfiling:
                result = cProfile.runctx("export( self, scene, placeholders)", globals(), locals())
            else:
                result = export(self, scene, placeholders)
            # Clean up the scene
            bpy.data.objects.remove(crv_ob)
            bpy.data.curves.remove(crv)
        else:
            if EnableProfiling:
                result = cProfile.runctx("export( self, scene, placeholders)", globals(), locals())
            else:
                result = export(self, scene, placeholders)

        CrnInfo("Total export time: %.2f" % (time.time() - start))

        if result != None:
            CrnInfo("Failed to export: %s" % result)
            self.report({'WARNING'}, result)
            bpy.context.window_manager['corona'] = {'msg': result}
            return { 'CANCELLED' }

        return { 'FINISHED' }


#--------------------------------
# Operator for writing .mtl files
class CoronaMTLWrite(bpy.types.Operator):
    '''Export Corona .mtl file only'''
    bl_label = "Export Materials"
    bl_idname = "corona.export_mat"

    def execute(self, context):
        textures_set = set()
        scene = context.scene
        filepath = os.path.join(realpath(scene.corona.export_path), scene.name.replace(' ', '_').replace(":", "_") + ".mtl")
        image_path = realpath(scene.corona.export_path)

        CrnUpdate("Writing material file ", scene.name.replace(' ', '_').replace(":", "_") + ".mtl")

        # Collect the materials for export to .mtl
        materials = set()
        node_mats = set()
        rendertypes = ['MESH', 'SURFACE', 'META', 'TEXT', 'CURVE']

        for mat in bpy.data.materials:
            crn_mat = mat.corona
            if crn_mat.node_tree != '' and crn_mat.node_output is not None:
                node_mats.add((mat.name, bpy.data.node_groups[ crn_mat.node_tree].nodes[ crn_mat.node_output]))
            else:
                materials.add(mat.name)

        # Open the file and write header.
        mat_file = open(filepath, "wb")
        fw = mat_file.write

        root = Element('mtlLib')

        root.append(Comment('# Blender MTL File: %r' % (os.path.basename(bpy.data.filepath) or "None")))
        root.append(Comment('# Material Count: {}'.format(len(materials) if len(materials) > 0 else 1)))
        version = get_version_string()
        time_stamp = get_timestamp()
        root.append(Comment('# Generated by %s addon, %s, %s' % (script_name, version, time_stamp)))

        # Write a default Corona material first, for any objects that have no material assigned.
        # fw('\nnewmtl None\n')
        # fw('Kd 0.8 0.8 0.8\n')

        if scene.corona.material_override:
            mat = None
            default_mat = False
            if scene.corona.clay_render:
                default_mat = True
            else:
                debug(bpy.data.materials)
                mat = bpy.data.materials[scene.corona.override_material]
            write_mtl( root, default_mat, mat, 'Default', True )
        else:
            write_mtl( root, True, None, 'Default', True )

        if len(materials) > 0:
            for mtl_mat_name in materials:
                default_mat = False
                mat = bpy.data.materials[mtl_mat_name]
                # fw('\nnewmtl %s\n' % mtl_mat_name)
                if scene.corona.material_override:
                    if scene.corona.clay_render:
                        default_mat = True
                    else:
                        mat = bpy.data.materials[scene.corona.override_material]
                #Write the material settings
                # if mat:
                write_mtl( root, default_mat, mat, mtl_mat_name, True)

        #Test if we're using node materials
        if len(node_mats) > 0:
            for mtl_mat_name, node in node_mats:
                default_mat = False
                mat = bpy.data.materials[mtl_mat_name]
                # fw('\nnewmtl %s\n' % mtl_mat_name)
                if scene.corona.material_override:
                    if scene.corona.clay_render:
                        default_mat = True
                    else:
                        mat = bpy.data.materials[scene.corona.override_material]

                write_mtl( root, default_mat, mat, mtl_mat_name, True)

        fw(prettify(root))

        mat_file.close()
        return {'FINISHED'}

#------------------------------------------
class CoronaSCNExport(bpy.types.Operator):
    '''Export Corona configuration files only'''
    bl_label = "Export Configuration"
    bl_idname = "corona.export_scene"

    # Local mode.
    _local_mode = False
    _local_layers = None

    # Collect dupli objects/instances.
    _dupli_obs = []
    _psys_obs = {}
    # Collect external .obj files used as instances.
    _ext_obs = []
    _ext_psys_obs = []
    # A set of all instanced objects.
    _no_export = set()

    def execute(self, context):
        scene = context.scene
        crn_scn = scene.corona
        camera = scene.camera

        scn_filename = scene.name.replace(' ', '_').replace(":", "_") + '.scn'
        conf_filename= scene.name.replace(' ', '_').replace(":", "_") + '.conf'
        scn_file = os.path.join(realpath(crn_scn.export_path), scn_filename)
        conf_file = os.path.join(realpath(crn_scn.export_path), conf_filename)

        #Open and write the .conf file
        conf_success = write_conf(context, conf_file)
        if not conf_success:
            self.report({'ERROR'}, "Something went wrong, unable to write .conf file. Check directory permissions of export directory.")
            return {'CANCELLED'}

        # Check if using local mode.

        for window in context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    space = area.spaces[0]
                    self._local_layers = space.layers_local_view[:]
                    break
        self._local_mode = self._local_layers is not None and True in self._local_layers

        # Open and write the .scn file.
        scn_success = write_scn( self, context, scn_file, conf_filename)
        if not scn_success:
            self.report({'ERROR'}, "Something went wrong, unable to write .scn file. Check directory permissions of export directory.")
            return {'CANCELLED'}

        return {'FINISHED'}

def register():
    bpy.utils.register_class( CoronaExportOperator)
    bpy.utils.register_class( CoronaMTLWrite)
    bpy.utils.register_class( CoronaSCNExport)
def unregister():
    bpy.utils.unregister_class( CoronaExportOperator)
    bpy.utils.unregister_class( CoronaMTLWrite)
    bpy.utils.unregister_class( CoronaSCNExport)
