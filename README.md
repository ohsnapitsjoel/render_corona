### Documentation ###
[Release Notes For Exporter](https://bitbucket.org/coronablender/render_corona/wiki/Release%20Notes)

[Corona Standalone Documentation](https://corona-renderer.com/wiki/standalone)

### Forums ###

https://corona-renderer.com/forum/index.php/board,28.0.html

### Version 3.0.0+ requires a new corona release! Blender 2.77+ ###
Exporter: [latest version](https://bitbucket.org/coronablender/render_corona/get/master.zip)

Corona [Build 2017-01-17](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AABF4EHPQ6ihxVIZRU1VtBPza/Standalone/2017-01-17%20-%20release%20and%20assert%2C%20fullspeed%20and%20legacy/fullspeed?dl=1)
Corona for older CPU's [Legacy 2017-01-17](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AADxhAVXP4TjcORPqZ3cYSKBa/Standalone/2017-01-17%20-%20release%20and%20assert%2C%20fullspeed%20and%20legacy/legacy?dl=1)

### Versions up to 2.4.13 Blender 2.77+ ###
Exporter: [v2.4.13](https://bitbucket.org/coronablender/render_corona/get/v2.4.13.zip)

Corona: [Build 2016-10-24](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AAA7gpdtb22Veqi4tzhJPjUka/Standalone/2016-10-24?dl=0)
### Other versions ###

Newer versions of Corona Standalone might be published here: [Standalone](https://www.dropbox.com/sh/kgzu0cqy903ygmb/AAA1CTqR1Uannm_D0J9JSo1Ga/Standalone?dl=0) or follow the daily build links from here: [Corona Doco](https://coronarenderer.freshdesk.com/support/solutions/articles/5000570015-daily-builds)

### Installation ###
Put into your blender/scripts/addons directory and configure using the installation steps from here: [Installation Instructions](https://corona-renderer.com/wiki/blender2corona/installation)


Due to popular demand if you want to gift a beer or two you can do so here: paypal.me/GlenBlanchard and if that doesn't work and you still want to this might work https://www.gofundme.com/coronablender