@Echo off


set /p oldversion=<version.txt
if "%1" == "" goto help

for /f "tokens=1,2,3 delims=." %%a in ("%oldversion%") DO set amajor=%%a&set aminor=%%b&set abuild=%%c
for /f "tokens=1,2,3 delims=." %%a in ("%1") DO set bmajor=%%a&set bminor=%%b&set bbuild=%%c

echo Old Version: %amajor%.%aminor%.%abuild%
echo New Version: %bmajor%.%bminor%.%bbuild%

fnr.exe --cl --find "%oldversion%" --replace "%1" --dir "%~dp0 " --fileMask "version.txt" --silent
fnr.exe --cl --find "\"version\": (%amajor%, %aminor%, %abuild%)" --replace "\"version\": (%bmajor%, %bminor%, %bbuild%)" --dir "%~dp0 " --fileMask "__init__.py" --silent
rem git tag v%1

goto end

:help

echo Pass in next version e.g. setversion 2.0.1
echo Current Version: %oldversion%

:end
