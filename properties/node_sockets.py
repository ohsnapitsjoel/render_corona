import bpy
from bpy.types import NodeSocketShader, NodeSocket, CurveMapping, PropertyGroup
from bpy.props import IntProperty, FloatProperty, FloatVectorProperty, EnumProperty, PointerProperty
from ..util    import strip_spaces, join_params, join_names_underscore, debug
from .material import CoronaMatProps
from .object   import CoronaObjects

import xml.etree.ElementTree
from xml.etree.ElementTree import Element, Comment, SubElement

class CoronaUVWSocket( NodeSocketShader):
    bl_idname = "CoronaUVW"
    bl_label = "UVW"

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color(self, context, node):
        return (0.8, 0.5, 0.5, 1)

# Socket base class.
class CoronaSocket( NodeSocket):
    bl_idname = "Corona"
    bl_label = ""

    # Set to default None.
    socket_val = None

    def get_socket_value( self, node, root):
        ''' Method to return socket's value as a string'''
        return "%.3f" % ( self.socket_val)

    def get_socket_params( self, node, root, inline = False):
        '''
        Method to return socket's values as a list.
        Function will only be called by the node if the socket is linked.
        '''
        linked_node = self.links[0].from_node
        # Return the parameters of the linked node as a list.
        node_params = linked_node.get_node_params(root, inline)
        return node_params

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Socket base class for :solid color properties.
class CoronaSolidSocket( NodeSocket):

    socket_val = None

    def get_socket_value( self, name, root):
        ''' Method to return socket's value as a string'''
        # debug(self.socket_val, str(type(self.socket_val)), len(self.socket_val))
        try:
            length = len(self.socket_val)
            if length == 1:
                return '%.3f' % self.socket_val[0]
            elif length == 2:
                return '%.3f %.3f' % (self.socket_val[0], self.socket_val[1])
            elif length == 3:
                return '%.3f %.3f %.3f' % (self.socket_val[0], self.socket_val[1], self.socket_val[2])
            elif length == 4:
                return '%.3f %.3f %.3f %.3f' % (self.socket_val[0], self.socket_val[1], self.socket_val[2], self.socket_val[3])
        except:
            pass

        return'%.3f' % self.socket_val

    def get_socket_params( self, name, root, inline = False):
        '''
        Method to return socket's values as a list.
        Function will only be called by the node if the socket is linked.
        '''
        linked_node = self.links[0].from_node
        # Return the parameters of the linked node.
        node_params = linked_node.get_node_params(root, inline)
        # map_name = join_names_underscore( name, self.name)
        # map_directive = 'newmap %s :%s\n' % ( map_name, node_name)
        # node_params.append( map_directive)
        return node_params

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.3, 0.8, 0.3, 1)


class CoronaAoDistMapSocket( CoronaSolidSocket):
    bl_idname = "CoronaAoDistMap"
    socket_val = FloatVectorProperty( name = "Distance Map",
                                description = "Texture by which to multiply the distance value, allowing it to change spatially",
                                default = (1, 1, 1, 1),
                                size = 4,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')
    def get_socket_value( self, name, root):
        return None

# Common sockets.
class CoronaFloatSocket( CoronaSocket):
    bl_idname = "CoronaFloat"
    bl_label = "Level"
    socket_val = FloatProperty( name = "Color",
                                default = 1)

# class CoronaIntSocket( CoronaSocket):
#     bl_idname = "CoronaInt"
#     bl_label = "Level"
#     socket_val = IntProperty( name = "Level",
#                                 default = 0)

#     def get_socket_value( self, node, root):
#         ''' Method to return socket's value as a string'''
#         return "%d" % ( self.socket_val)

class CoronaFloatSocket010( CoronaSocket):
    bl_idname = "CoronaFloat010"
    bl_label = "Level"
    socket_val = FloatProperty( name = "Color",
                                min = 0,
                                max = 1,
                                default = 0)

class CoronaFloatSocket011( CoronaSocket):
    bl_idname = "CoronaFloat011"
    bl_label = "Level"
    socket_val = FloatProperty( name = "Color",
                                min = 0,
                                max = 1,
                                default = 1)

class CoronaColorSocket( CoronaSolidSocket):
    bl_idname = "CoronaColor"
    socket_val = FloatVectorProperty( name = "Color",
                                default = (1, 1, 1, 1),
                                size = 4,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaMultiplySocket( CoronaSolidSocket):
    bl_idname = "CoronaMultiply"
    socket_val = FloatVectorProperty( name = "Multiply",
                                default = (1, 1, 1, 1),
                                size = 4,
                                min = 0,
                                soft_max = 100,
                                subtype = 'NONE')

class CoronaLinkedMaterialSocket( CoronaSolidSocket):
    bl_idname = "CoronaLinkedMaterial"
    bl_label = "Material"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def get_socket_value( self, node, root):
        return None

    def draw_color( self, context, node):
        return (0.8, 0.3, 0.8, 1)

class CoronaLinkedMapSocket( CoronaSolidSocket):
    bl_idname = "CoronaLinkedMap"
    bl_label = "UV Link"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def get_socket_value( self, node, root):
        return None

class CoronaLinkedSocket( CoronaSocket):
    bl_idname = "CoronaLinked"
    bl_label = "UV Link"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def get_socket_value( self, node, root):
        return None

    def draw_color( self, context, node):
        return (0.8, 0.5, 0.5, 1)

# class CoronaLayeredMaterialSocket( CoronaSolidSocket):
#     bl_idname = "CoronaLayeredMaterial"
#     bl_label = "Material"

#     weight = FloatProperty( name = "Weight",
#                                 description = "",
#                                 default = 1,
#                                 min = 0, max = 1)

#     def draw( self, context, layout, node, text):
#         layout.prop( self, "weight", text)

#     def get_socket_value( self, node, root):
#         return self.weight

#     def draw_color( self, context, node):
#         return (0.8, 0.3, 0.8, 1)

# Mix shader sockets.
class CoronaLayeredWeightSocket( CoronaSolidSocket):
    bl_idname = "CoronaLayeredWeight"
    bl_label = "Weight"

    socket_val = FloatProperty( name = "Weight",
                                description = "Color / texture by which to mix two input colors",
                                default = 0.5,
                                min = 0, max = 1)

# Mix shader sockets.
class CoronaMixAmountSocket( CoronaSolidSocket):
    bl_idname = "CoronaMixAmount"
    bl_label = "Amount"

    socket_val = FloatVectorProperty( name = "Amount",
                                description = "Color / texture by which to mix two input colors",
                                default = (1, 1, 1),
                                min = 0, max = 1,
                                subtype = 'COLOR')

# Ray switch shader sockets.
class CoronaRaySwitchSocket( CoronaSolidSocket):
    bl_idname = "CoronaRaySwitchColor"
    bl_label = "Ray Switch Color"

    socket_val = FloatVectorProperty( name = "Color",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

# Fresnel shader sockets.
class CoronaFresnelChildSocket( CoronaSolidSocket):
    bl_idname = "CoronaFresnelChild"
    bl_label = "Child"

    socket_val = FloatVectorProperty( name = "Color",
                                description = "Applies a fresnel curve on mono input provided by a child node",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

# Fresnel shader sockets.
class CoronaFresnelPerpSocket( CoronaSolidSocket):
    bl_idname = "CoronaFresnelPerp"
    bl_label = "Perpendicular"

    socket_val = FloatVectorProperty( name = "Color",
                                description = "Color / texture used for grazing angles",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaFresnelParSocket( CoronaSolidSocket):
    bl_idname = "CoronaFresnelPar"
    bl_label = "Parallel"

    socket_val = FloatVectorProperty( name = "Color",
                                description = "Color / texture used for rays directly facing the camera",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

# Material output node sockets.
# Diffuse.
class CoronaDiffuseLevelSocket( CoronaSocket):
    bl_idname = "CoronaDiffuseLevel"
    bl_label = "Diffuse Level"
    socket_val = CoronaMatProps.diffuse_level

class CoronaKdSocket( CoronaSocket):
    bl_idname = "CoronaKd"
    bl_label = "Kd"

    socket_val = CoronaMatProps.kd

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs["Diffuse Level"].socket_val
        # debug("Level:", node.inputs["Diffuse Level"].socket_val)
        return '%.3f %.3f %.3f' % (self.socket_val[0] * mult,
                                      self.socket_val[1] * mult,
                                      self.socket_val[2] * mult)

# Translucency.
class CoronaTranslucencyLevelSocket( CoronaSocket):
    bl_idname = "CoronaTranslucencyLevel"
    bl_label = "Translucency Level"

    socket_val = CoronaMatProps.translucency_level

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = "Translucency Level")

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.3f' % self.socket_val


class CoronaTranslucencySocket( CoronaSocket):
    bl_idname = "CoronaTranslucency"
    bl_label = "Translucency"

    socket_val =  CoronaMatProps.translucency

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.3f %.3f %.3f' % (self.socket_val[0],
                                                self.socket_val[1],
                                                self.socket_val[2])
# Reflection.
class CoronaKsSocket( CoronaSocket):
    bl_idname = "CoronaKs"
    bl_label = "Ks"

    socket_val =  CoronaMatProps.ks

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs['Reflect Level'].socket_val
        return '%.3f %.3f %.3f' % ( self.socket_val[0] * mult,
                                         self.socket_val[1] * mult,
                                         self.socket_val[2] * mult)

class CoronaReflectLevelSocket( CoronaSocket):
    bl_idname = "CoronaReflectLevel"
    bl_label = "Level"

    socket_val = CoronaMatProps.ns

    def draw( self, context, layout, node, text):
        layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaReflectGlossSocket( CoronaSocket):
    bl_idname = "CoronaReflectGloss"
    bl_label = "Glossiness"

    socket_val =  CoronaMatProps.reflect_glossiness

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaReflectFresnelSocket( CoronaSocket):
    bl_idname = "CoronaReflectFresnel"
    bl_label = "Fresnel"

    socket_val =  CoronaMatProps.reflect_fresnel

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaAnisotropySocket( CoronaSocket):
    bl_idname = "CoronaAnisotropy"
    bl_label = "Anisotropy"

    socket_val = CoronaMatProps.anisotropy

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    # def get_socket_value( self, node, root):
    #     aniso_rot = node.inputs['Anisotropy Rotation'].socket_val
    #     return '%.3f %.3f' % ( self.socket_val, aniso_rot)

class CoronaAnisotropyRotSocket( CoronaSocket):
    bl_idname = "CoronaAnisotropyRot"
    bl_label = "Anisotropy Rotation"

    socket_val = CoronaMatProps.aniso_rotation

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Refraction.
class CoronaNiSocket( CoronaSocket):
    bl_idname = "CoronaRefractIOR"
    bl_label = "IOR"

    socket_val = CoronaMatProps.ni

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaRefractColorSocket( CoronaSocket):
    bl_idname = "CoronaRefractColor"
    bl_label = "Color"

    socket_val =  CoronaMatProps.refract

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs['Refract Level'].socket_val
        return '%.3f %.3f %.3f' % ( self.socket_val[0] * mult,
                                              self.socket_val[1] * mult,
                                              self.socket_val[2] * mult)

class CoronaRefractGlossSocket( CoronaSocket):
    bl_idname = "CoronaRefractGloss"
    bl_label = "Glossiness"

    socket_val =  CoronaMatProps.refract_glossiness

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaRefractModeSocket( CoronaSocket):
    bl_idname = "CoronaRefractMode"
    bl_label = "Mode"

    socket_val = EnumProperty( name = "Mode",
                                description = "Refraction mode",
                                items = [
                                ('thin', "Thin", "Thin - no refraction"),
                                ('caustics', "Caustics", "Enable refractive caustics (slow)"),
                                ('hybrid', "Hybrid", "Hybrid mode of computing caustics only for direct rays")],
                                default = 'hybrid')

    def draw( self, context, layout, node, text):
        layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%s' % self.socket_val

class CoronaRefractLevelSocket( CoronaSocket):
    bl_idname = "CoronaRefractLevel"
    bl_label = "Level"

    socket_val = CoronaMatProps.refract_level

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaAbsorptionDistSocket( CoronaSocket):
    bl_idname = "CoronaAbsorptionDist"
    bl_label = "Absorption Distance"

    socket_val = CoronaMatProps.absorption_distance

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0, 0, 0, 0)

class CoronaAbsorptionColorSocket( CoronaSocket):
    bl_idname = "CoronaAbsorptionColor"
    bl_label = "Absorption Color"

    socket_val = CoronaMatProps.absorption_color

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.3f %.3f %.3f' % ( self.socket_val[0],
                                                       self.socket_val[1],
                                                       self.socket_val[2])

# Emission.
class CoronaEmissionGlossSocket( CoronaSocket):
    bl_idname = "CoronaEmissionGloss"
    bl_label = "Emission Glossiness"

    socket_val =  CoronaMatProps.emission_gloss

    def draw( self, context, layout, node, text):

        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaKeSocket( CoronaSocket):
    bl_idname = "CoronaKe"
    bl_label = "Emission Color"

    socket_val =  CoronaMatProps.ke

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs['Multiplier'].socket_val
        return '%.3f %.3f %.3f' % ( self.socket_val[0] * mult,
                                         self.socket_val[1] * mult,
                                         self.socket_val[2] * mult)

class CoronaEmissionMultSocket( CoronaSocket):
    bl_idname = "CoronaEmissionMult"
    bl_label = "Emission Multiplier"

    socket_val = CoronaMatProps.emission_mult

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return ''

class CoronaEmissionMultSocketZero( CoronaSocket):
    bl_idname = "CoronaEmissionMultZero"
    bl_label = "Emission Multiplier"

    socket_val = CoronaMatProps.emission_mult_zero

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return ''


class CoronaPointSocket( CoronaSocket):
    bl_idname = "CoronaPoint"
    bl_label = "Point"

    socket_x = FloatProperty( name = "X",
                                min = 0,
                                max = 1,
                                default = 0)
    socket_y = FloatProperty( name = "Y",
                                min = 0,
                                max = 1,
                                default = 0)

    def draw( self, context, layout, node, text):
        row = layout.row()
        row.prop( self, "socket_x", text = "X")
        row.prop( self, "socket_y", text = "Y")

    def draw_color( self, context, node):
        return (0.1, 0.1, 0.1, 0.1)

    def get_socket_value( self, node, root):
        xml_point = Element('point')
        xml_point.text = '%.4f' % self.socket_y
        xml_point.set("position", '%.4f' % self.socket_x)
        return xml_point

class CoronaOpacitySocket( CoronaSocket):
    bl_idname = "CoronaOpacity"
    bl_label = "Opacity"

    socket_val =   CoronaMatProps.opacity

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.3f %.3f %.3f' % ( self.socket_val[0],
                                              self.socket_val[1],
                                              self.socket_val[2])

# Normal socket.
class CoronaNormalSocket( CoronaSocket):
    bl_idname = "CoronaNormal"
    bl_label = "Normal"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.67, 0.45, 1, 1)

# Bump socket.
class CoronaBumpSocket( CoronaSocket):
    bl_idname = "CoronaBump"
    bl_label = "Bump"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaObjectListSocket( CoronaSocket):
    bl_idname = "CoronaObjectList"
    bl_label = "Objects"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (1, 0.2, 0, 1)

    def get_socket_value( self, node, root):
        return self.socket_val

# Factor socket.
class CoronaFactorSocket( CoronaSocket):
    bl_idname = "CoronaFactor"
    bl_label = "Factor"

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Scattering albedo socket.
class CoronaScatteringAlbedoSocket( CoronaSocket):
    bl_idname = "CoronaScattering"
    bl_label = "Scattering"

    socket_val = CoronaMatProps.scattering_albedo

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.3f %.3f %.3f' % ( self.socket_val[0],
                                              self.socket_val[1],
                                              self.socket_val[2])

# Scattering albedo socket.
class CoronaMeanCosineSocket( CoronaSocket):
    bl_idname = "CoronaMeanCosine"
    bl_label = "Directionality"

    socket_val = CoronaMatProps.mean_cosine

    def draw( self, context, layout, node, text):
        layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0, 0, 0, 0)

    def get_socket_value( self, node, root):
        return '%.3f' % ( self.socket_val)

def register():
    # bpy.utils.register_class( CoronaColorSocket)
    # bpy.utils.register_class( CoronaAbsorptionColorSocket)
    # bpy.utils.register_class( CoronaAbsorptionDistSocket)
    # bpy.utils.register_class( CoronaMeanCosineSocket)
    # bpy.utils.register_class( CoronaScatteringAlbedoSocket)
    # bpy.utils.register_class( CoronaRefractLevelSocket)
    # bpy.utils.register_class( CoronaRefractModeSocket)
    # bpy.utils.register_class( CoronaRefractGlossSocket)
    # bpy.utils.register_class( CoronaRefractColorSocket)
    # bpy.utils.register_class( CoronaNiSocket)
    # bpy.utils.register_class( CoronaAnisotropyRotSocket)
    # bpy.utils.register_class( CoronaAnisotropySocket)
    # bpy.utils.register_class( CoronaReflectFresnelSocket)
    # bpy.utils.register_class( CoronaReflectGlossSocket)
    # bpy.utils.register_class( CoronaReflectLevelSocket)
    # bpy.utils.register_class( CoronaKsSocket)
    # bpy.utils.register_class( CoronaTranslucencySocket)
    # bpy.utils.register_class( CoronaKdSocket)
    # bpy.utils.register_class( CoronaEmissionGlossSocket)
    # bpy.utils.register_class( CoronaEmissionMultSocket)
    # bpy.utils.register_class( CoronaKeSocket)
    # bpy.utils.register_class( CoronaOpacitySocket)
    # bpy.utils.register_class( CoronaMixASocket)
    # bpy.utils.register_class( CoronaMixBSocket)
    # bpy.utils.register_class( CoronaMixAmountSocket)
    # bpy.utils.register_class( CoronaFresnelParSocket)
    # bpy.utils.register_class( CoronaFresnelPerpSocket)
    # bpy.utils.register_class( CoronaBumpSocket)
    # bpy.utils.register_class( CoronaDiffuseLevelSocket)
    # bpy.utils.register_class( CoronaTranslucencyLevelSocket)
    # bpy.utils.register_class( CoronaRaySwitchSocket)
    # bpy.utils.register_class( CoronaFactorSocket)
    bpy.utils.register_class( CoronaSolidSocket)
    bpy.utils.register_class( CoronaUVWSocket)

def unregister():
    # bpy.utils.unregister_class( CoronaColorSocket)
    # bpy.utils.unregister_class( CoronaAbsorptionColorSocket)
    # bpy.utils.unregister_class( CoronaAbsorptionDistSocket)
    # bpy.utils.unregister_class( CoronaMeanCosineSocket)
    # bpy.utils.unregister_class( CoronaScatteringAlbedoSocket)
    # bpy.utils.unregister_class( CoronaRefractLevelSocket)
    # bpy.utils.unregister_class( CoronaRefractModeSocket)
    # bpy.utils.unregister_class( CoronaRefractGlossSocket)
    # bpy.utils.unregister_class( CoronaRefractColorSocket)
    # bpy.utils.unregister_class( CoronaNiSocket)
    # bpy.utils.unregister_class( CoronaAnisotropyRotSocket)
    # bpy.utils.unregister_class( CoronaAnisotropySocket)
    # bpy.utils.unregister_class( CoronaReflectFresnelSocket)
    # bpy.utils.unregister_class( CoronaReflectGlossSocket)
    # bpy.utils.unregister_class( CoronaReflectLevelSocket)
    # bpy.utils.unregister_class( CoronaKsSocket)
    # bpy.utils.unregister_class( CoronaTranslucencySocket)
    # bpy.utils.unregister_class( CoronaKdSocket)
    # bpy.utils.unregister_class( CoronaEmissionGlossSocket)
    # bpy.utils.unregister_class( CoronaEmissionMultSocket)
    # bpy.utils.unregister_class( CoronaKeSocket)
    # bpy.utils.unregister_class( CoronaOpacitySocket)
    # bpy.utils.unregister_class( CoronaMixASocket)
    # bpy.utils.unregister_class( CoronaMixBSocket)
    # bpy.utils.unregister_class( CoronaMixAmountSocket)
    # bpy.utils.unregister_class( CoronaFresnelParSocket)
    # bpy.utils.unregister_class( CoronaFresnelPerpSocket)
    # bpy.utils.unregister_class( CoronaBumpSocket)
    # bpy.utils.unregister_class( CoronaDiffuseLevelSocket)
    # bpy.utils.unregister_class( CoronaTranslucencyLevelSocket)
    # bpy.utils.unregister_class( CoronaRaySwitchSocket)
    # bpy.utils.unregister_class( CoronaFactorSocket)
    bpy.utils.unregister_class( CoronaSolidSocket)
    bpy.utils.unregister_class( CoronaUVWSocket)
