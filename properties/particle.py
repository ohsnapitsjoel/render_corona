import bpy
from bpy.props import BoolProperty, FloatProperty, IntProperty, EnumProperty, PointerProperty

#------------------------------------
# Particle system properties
#------------------------------------
class CoronaPsysProps( bpy.types.PropertyGroup):
    shape = EnumProperty( name = "Strand Shape",
                                        description = "Strand shape to use for rendering",
                                        items = [
                                        ( 'thick', 'Thick', 'Use thick strands for rendering'),
                                        ( 'ribbon', 'Ribbon', 'Use ribbon strands for rendering - ignore thickness of strand'),],
                                        default = 'thick')

    export_color = EnumProperty( name = "Strand Color",
                                        description = "Strand color",
                                        items = [
                                        ( 'uv_texture_map', 'UV Texture Map', 'Use emitter\'s UV position and texture map for strand color, the W goes from 0.0 at the base to 1.0 at the tip'),
                                        # ( 'vertex_color', 'Vertex Color', 'Use emitter\'s base vertex color')
                                        # ( 'strand', 'Strand', 'Each strand has a constant U of 0 and a V progressing from 0.0 at the base to 1.0 at the tip')
                                        ( 'none', 'None', 'Use base color')
                                        ],
                                        default = 'uv_texture_map')

    root_size = FloatProperty( name = "Root",
                                        description = "Thickness of strand root",
                                        default = 1.0,
                                        min = 0.0001,
                                        max = 100)

    tip_size = FloatProperty( name = "Tip",
                                        description = "Thickness of strand tip",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 100)

    resolution = IntProperty( name = "Resolution",
                                        description = "Cylindrical resolution of strand. Default of 0 should be sufficient in the majority of cases. Higher values require longer export times",
                                        default = 0,
                                        min = 0,
                                        max = 2)

    scaling = FloatProperty( name = "Scaling",
                                        description = "Multiplier of width properties",
                                        default = 0.01,
                                        min = 0.0,
                                        max = 1000)

    close_tip = BoolProperty( name = "Close Tip",
                                        description = "Closed hair tip",
                                        default = True)

    hair_shape = FloatProperty( name = "Hair Shape",
                                        description = "-1 the hair thickness is constant thickness from root to tip, 0 hair thickness is evenly interpolated from root to tip, 1 hair starts at root size and quickly reaches tip size",
                                        default = 0.0,
                                        min = -1.0,
                                        max = 1.0)

def register():
    bpy.utils.register_class( CoronaPsysProps)
    bpy.types.ParticleSettings.corona = PointerProperty( type = CoronaPsysProps)
def unregister():
    bpy.utils.unregister_class( CoronaPsysProps)
