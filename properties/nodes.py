import bpy, math
from bpy.types          import NodeTree, Node
from bpy.app.handlers   import persistent
from nodeitems_utils    import NodeCategory, NodeItem
from ..util             import addon_dir, strip_spaces, realpath, join_names_underscore
from ..util             import filter_flatten_params, debug, CrnInfo, view3D_mat_update
from .material          import CoronaMatProps, CoronaTexProps
from .object            import CoronaObjects
import nodeitems_utils
import os
import mathutils

import xml.etree.ElementTree
from xml.etree.ElementTree import Element, Comment, SubElement

def getNodePtr(node):
    name_pointer = join_names_underscore( node.name, str(node.as_pointer()))
    if len(node.label) != 0:
        name_pointer = join_names_underscore( node.label, name_pointer)
    return name_pointer

def getMat(root, name, type = 'Native'):
    results = root.findall(".//*[@name='%s']" % name)
    # debug("Results", results)
    if len(results) == 0:
        xml_mat_def = Element('materialDefinition', name=name)
        xml_mat = SubElement(xml_mat_def, 'material')
        xml_mat.set('class', type)
        return xml_mat, xml_mat_def
    else:
        return None, None

def getMap(root, name, clazz):
    results = root.findall(".//*[@name='%s']" % name)
    if len(results) == 0:
        xml_map_def = Element('mapDefinition', name=name)
        xml_map = SubElement(xml_map_def, 'map')
        xml_map.set('class', clazz)
        return xml_map, xml_map_def
    else:
        return None, None

def addSocket(root, parent, node, name, xmlname, inline = False):
    socket = node.inputs[name]
    if socket.is_linked and len(socket.links) > 0:
        xml = SubElement(parent, xmlname)
        xml.append(socket.get_socket_params(node, root, inline))
        return xml
    else:
        val = socket.get_socket_value(node, root)
        if val != None:
            xml = SubElement(parent, xmlname)
            xml.text = str(val)
            return xml

def addChild(root, parent, node, name, inline = False):
    socket = node.inputs[name]
    if socket.is_linked:
        parent.append(socket.get_socket_params(node, root, inline))
    else:
        val = socket.get_socket_value(node, root)
        if val != None:
            parent.text = str(val)

def getReference(name):
    el = Element('material')
    el.text = name
    el.set('class', 'Reference')
    return el

def getMapReference(name):
    el = Element('map')
    el.text = name
    el.set('class', 'Reference')
    return el

#--------------------------------
# Corona node tree.
#--------------------------------
class CoronaNodeTree( NodeTree):
    '''Corona Node Editor2'''
    bl_idname = 'CoronaNodeTree'
    bl_label = 'Corona Node Tree'
    bl_icon = 'NODETREE'

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer == 'CORONA'

    # @classmethod
    # def get_from_context( cls, context):
    #     print("Getting from context")
    #     obj = context.object
    #     material = obj.active_material
    #     crn_mat = material.corona
    #     renderer = context.scene.render
    #     if renderer.engine == 'CORONA' and context.object is not None and crn_mat is not None and crn_mat.node_tree != '':
    #         return (crn_mat.node_tree)
    #         # area = bpy.context.area
    #         # oldtype = area.type
    #         # area.type = "NODE_EDITOR"
    #         # bpy.context.active_node = crn_mat.node_tree
    #         # area.type = oldtype
    #     return (bpy.context.active_node)

    @classmethod
    def get_from_context(cls, context):
        ob = context.active_object
        if ob: #and ob.type not in BlenderUtils.NonGeometryTypes
            mat = ob.active_material
            if mat and mat.corona.node_tree:
                ntree = bpy.data.node_groups[mat.corona.node_tree]
                return ntree, ob, None
        return (None, None, None)
#--------------------------------
# Base class for Corona nodes.
#--------------------------------
class CoronaNode:
    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return context.bl_idname == "CoronaNodeTree" and renderer == 'CORONA'

    def draw_buttons( self, context, layout):
        pass

    def draw_buttons_ext(self, context, layout):
        pass

    def copy( self, node):
        pass

    def free( self):
        CrnInfo( "Removing Corona node '%s'" % self.bl_label)

    def update( self):
        # debug( "Update Corona node", self, bpy.context.object)
        view3D_mat_update(self, bpy.context)

    # def socket_value_update( self, context):
    #     CrnInfo( "Socket value update Corona node '%s'" % self.bl_label)

    def draw_label( self):
        return self.bl_label


#--------------------------------
# Image texture shader node.
#--------------------------------
class CoronaTexNode( Node, CoronaNode):
    '''Corona Image Texture Node'''
    bl_idname = "CoronaTexNode"
    bl_label = "Image Texture"
    bl_icon = 'TEXTURE'

    tex_path = bpy.props.StringProperty( name = "Texture",
                                         description = "Path to the image texture",
                                         default = '',
                                         subtype = 'FILE_PATH')
    use_gamma = bpy.props.BoolProperty( name = "Use Gamma",
                                        description = "When disabled will automatically use 2.2 for LDR and 1.0 for HDR",
                                        default = False)

    gamma = bpy.props.FloatProperty( name = "Gamma",
                                     default = 2.2)

    def init( self, context):
        self.inputs.new( 'CoronaFloat', 'Bump Strength')
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.label("Image:")
        layout.prop( self, "tex_path", text = "")
        layout.prop( self, "use_gamma")
        if self.use_gamma:
            layout.prop( self, "gamma")

    def get_node_params( self, root, inline = False):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        if self.tex_path != '':
            name_pointer = getNodePtr(self)
            xml_mat, xml_mat_def = getMap(root, name_pointer, 'Texture')
            if xml_mat != None:
                SubElement(xml_mat, 'image').text = realpath(self.tex_path)
                if self.use_gamma:
                    SubElement(xml_mat, 'gamma').text = '%.3f' % (self.gamma)
                addSocket(root, xml_mat, self, "Bump Strength", "bumpStrength", inline)
                addChild(root, xml_mat, self, "UV Map", True)
                if not inline: root.append(xml_mat_def)

            if inline:
                return xml_mat
            else:
                return getMapReference(name_pointer)
        return None

class CoronaFalloffNode( Node, CoronaNode):
    '''Corona Falloff Node'''
    bl_idname = "CoronaFalloffNode"
    bl_label = "Falloff"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Falloff Mode",
                                        description = "Which geometry frame is the Axis defined in",
                                        default = 'view',
                                        items = [
                                            ('view', 'View', 'Axis is relative to camera using this the axis is usually 0 0 1'),
                                            ('local', 'Local', 'Axis is local relative to object'),
                                            ('world', 'World', 'Axis is world relative')])

    axis = bpy.props.FloatVectorProperty( name = "Axis",
                                description = "",
                                default = (0.0, 0.0, 1.0),
                                subtype = "TRANSLATION")

    abs_value = bpy.props.BoolProperty( name = "Absolute Value",
                                    description = "",
                                    default = False)

    def init( self, context):
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode", text = "Mode")
        layout.prop( self, "axis", text = "Axis")
        layout.prop( self, "abs_value", text = "Absolute Value")

    def get_node_params( self, root, inline = False):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Falloff')
        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            SubElement(xml_mat, 'axis').text = "%.3f %.3f %.3f" % self.axis[:]
            SubElement(xml_mat, 'absValue').text = "True" if self.abs_value else "False"
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaDataNode( Node, CoronaNode):
    '''Corona Data Node'''
    bl_idname = "CoronaDataNode"
    bl_label = "Data"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Data Mode",
                                        description = "Geometry data recast as a color",
                                        default = 'UVW',
                                        items = [
                                            ('UVW', 'View', ''),
                                            ('shadingNormal', 'Shading Normal', ''),
                                            ('geometryNormal', 'Geometry Normal', ''),
                                            ('dotProduct', 'Dot Product', ''),
                                            ('zDepth', 'Z Depth', ''),
                                            ('dUvw', 'D UVW', '')])

    def init( self, context):
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode", text = "Mode")

    def get_node_params( self, root, inline = False):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Data')
        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaGradientNode( Node, CoronaNode):
    '''Corona Gradient Node'''
    bl_idname = "CoronaGradientNode"
    bl_label = "Gradient"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Gradient Mode",
                                        description = "",
                                        default = 'radial',
                                        items = [
                                            ('u', 'U', 'Creates a gradient in the u axis by directly returning the u value as intensity'),
                                            ('v', 'V', 'Creates a gradient in the v axis by directly returning the v value as intensity'),
                                            ('radial', 'Radial', 'Creates a radial gradient in the 0-1 UVW square with center in 0.5, 0.5')])

    def init( self, context):
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode", text = "")

    def get_node_params( self, root, inline = False):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Gradient')
        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaRoundEdgesNode( Node, CoronaNode):
    '''Corona RoundEdges Node'''
    bl_idname = "CoronaRoundEdgesNode"
    bl_label = "Round Edges"
    bl_icon = 'SMOOTH'

    max_radius = bpy.props.FloatProperty( name = "Max radius",
                                         description = "Radius of the rounded corners effect",
                                         min = 0,
                                         default = 0)
    samples = bpy.props.IntProperty( name = "Samples",
                                     description = "Number of samples to take. It influences the render noise and speed.",
                                     default = 0,
                                     min = 0)

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMap', 'Mapped Max Radius')
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketColor', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "max_radius", text = "Max Radius")
        layout.prop( self, "samples", text = "Samples")

    def get_node_params( self, root, inline = False):
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'RoundEdges')
        if xml_mat != None:
            SubElement(xml_mat, 'maxRadius').text = "%.3f" % self.max_radius
            if self.samples != 0:
                SubElement(xml_mat, 'samples').text = "%d" % self.samples
            if self.inputs["Mapped Max Radius"].is_linked == True:
                addChild(root, xml_mat, self, "Mapped Max Radius", True)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)


#--------------------------------
# AO shader node.
#--------------------------------
class CoronaAONode( Node, CoronaNode):
    '''Corona AO Shader Node'''
    bl_idname = "CoronaAONode"
    bl_label = "AO Shader"
    bl_icon = 'SMOOTH'

    distance = bpy.props.FloatProperty( name = "Distance",
                                        description = "Maximum distance rays travel before being considered not occluded",
                                        default = 10,
                                        min = 0,
                                        soft_max = 100)

    # invert = bpy.props.BoolProperty( name = "Invert",
    #                                     description = "Causes the rays to be shot below normal, not above, resulting in computing the occlusion below normal",
    #                                     default = False)

    samples = bpy.props.IntProperty( name = "Samples",
                                        description = "Quality of ambient occlusion calculation",
                                        default = 50,
                                        min = 1,
                                        max = 2048)

    phong_exponent = bpy.props.FloatProperty( name = "Phong Exponent",
                                        description = "How concentrated should be the rays around normal",
                                        default = 1)

    mix_exponent = bpy.props.FloatProperty( name = "Mix Exponent",
                                        description = "Sets balance between occluded and unoccluded color (works similarly to gamma correction)",
                                        default = 1)

    normal_mode = bpy.props.EnumProperty( name = "Normal Mode",
                                        description = "Where to compute the effect - on the outside/inside of the object",
                                        default = 'both',
                                        items = [
                                            ('both', 'Both', 'Both sides used'),
                                            ('outside', 'Outside', 'Outside is used'),
                                            ('inside', 'Inside', 'Inside is used')])

    exclude_mode = bpy.props.EnumProperty( name = "Occlude Mode",
                                        description = "Self occlusion, other occlusion, include/exclude or all",
                                        default = 'all',
                                        items = [
                                            ('all', 'All', 'All objects contribute to occlusion results'),
                                            ('same', 'Same', 'Only self-occlusion'),
                                            ('other', 'Other', 'No self-occlusion'),
                                            ('list', 'List', 'Include/Exclude used for occlusion.')])

    offset = bpy.props.FloatVectorProperty( name = "Offset",
                                description = "Constant offset in world space added to all generated rays. This can be used to shift the distribution of rays in one constant direction (typically up or down to create for example water weathering)",
                                default = (0.0, 0.0, 0.0),
                                subtype = "TRANSLATION")

    def init( self, context):
        self.inputs.new( 'CoronaAoDistMap', "Distance Map")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaObjectList', "Include")
        self.inputs.new( 'CoronaObjectList', "Exclude")
        self.outputs.new( 'NodeSocketFloat', "Occluded")

    def draw_buttons( self, context, layout):
        # layout.prop( self, "invert")
        layout.prop( self, "samples")
        layout.prop( self, "distance")
        layout.prop( self, "phong_exponent")
        layout.prop( self, "mix_exponent")
        layout.prop( self, "normal_mode")
        layout.prop( self, "exclude_mode")
        layout.prop( self, "offset")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Ao')
        if xml_mat != None:
            SubElement(xml_mat, 'maxDistance').text = '%.3f' % self.distance
            # SubElement(xml_mat, 'invert').text = 'true' if self.invert else 'false'
            SubElement(xml_mat, 'samples').text = '%d' % self.samples
            SubElement(xml_mat, 'phongExponent').text = '%.3f' % self.phong_exponent
            SubElement(xml_mat, 'mixExponent').text = '%.3f' % self.mix_exponent
            if self.exclude_mode == 'same':
                SubElement(xml_mat, 'excludeMode').text = 'other'
            elif self.exclude_mode == 'other':
                SubElement(xml_mat, 'excludeMode').text = 'same'
            elif self.exclude_mode != 'all':
                SubElement(xml_mat, 'excludeMode').text = self.exclude_mode
            SubElement(xml_mat, 'normalMode').text = self.normal_mode
            SubElement(xml_mat, 'offset').text = '%.3f %.3f %.3f' % self.offset[0:3]
            addSocket(root, xml_mat, self, "Distance Map", "mappedDistance", inline)
            addChild(root, xml_mat, self, "UV Map", True)

            if self.inputs["Include"].is_linked or self.inputs["Exclude"].is_linked:
                xml_inex = SubElement(xml_mat, 'includeExclude')
                if self.inputs["Include"].is_linked:
                    includes = self.inputs["Include"].get_socket_params(self, root, inline)
                    for inc in includes:
                        SubElement(xml_inex, 'included').text = inc.name

                if self.inputs["Exclude"].is_linked:
                    excludes = self.inputs["Exclude"].get_socket_params(self, root, inline)
                    for excl in excludes:
                        SubElement(xml_inex, 'excluded').text = excl.name

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaUVWNode( Node, CoronaNode):
    '''Corona UV Shader Node'''
    bl_idname = "CoronaUVWNode"
    bl_label = "UV Map"
    bl_icon = 'SMOOTH'

    mode = CoronaTexProps.uv_mode
    map_channel = CoronaTexProps.uv_map_channel
    scale = CoronaTexProps.uv_scale
    offset = CoronaTexProps.uv_offset
    rotate_z = CoronaTexProps.uv_rotate_z
    enviro_rotate = CoronaTexProps.uv_enviro_rotate
    enviro_mode = CoronaTexProps.uv_enviro_mode
    wrap_mode_u = CoronaTexProps.uv_wrap_mode_u
    wrap_mode_v = CoronaTexProps.uv_wrap_mode_v
    blur = CoronaTexProps.uv_blur
    use_real_scale = CoronaTexProps.uv_use_real_scale

    def init( self, context):
        self.outputs.new( 'CoronaUVW', "UV Map")

    def draw_buttons( self, context, layout):
        layout.prop( self, "mode")
        layout.prop( self, "map_channel")
        layout.prop( self, "scale")
        layout.prop( self, "offset")
        layout.prop( self, "rotate_z")
        layout.prop( self, "enviro_rotate")
        layout.prop( self, "enviro_mode")
        layout.prop( self, "wrap_mode_u")
        layout.prop( self, "wrap_mode_v")
        layout.prop( self, "blur")
        layout.prop( self, "use_real_scale")

    def get_node_params( self, root, inline = False):

        # name_pointer = getNodePtr(self)
        # xml_mat, xml_mat_def = getMap(root, name_pointer, 'Mix')
        xml_uv = Element('uvMap')

        SubElement(xml_uv, 'mode').text = self.mode
        if self.map_channel != -1:
            SubElement(xml_uv, 'mapChannel').text = '%d' % self.map_channel
        SubElement(xml_uv, 'scale').text = '%.3f %.3f %.3f' % self.scale[0:3]
        SubElement(xml_uv, 'offset').text = '%.3f %.3f %.3f' % self.offset[0:3]
        SubElement(xml_uv, 'rotateZ').text = '%.3f' % (self.rotate_z * 180 / math.pi)
        SubElement(xml_uv, 'enviroRotate').text = '%.3f' % (self.enviro_rotate * 180 / math.pi + 180)
        SubElement(xml_uv, 'enviroMode').text = self.enviro_mode
        SubElement(xml_uv, 'wrapModeU').text = self.wrap_mode_u
        SubElement(xml_uv, 'wrapModeV').text = self.wrap_mode_v
        if self.blur != 0:
            SubElement(xml_uv, 'blur').text = '%.3f' % self.blur
        SubElement(xml_uv, 'useRealWorldScale').text = 'True' if self.use_real_scale else 'False'

        return xml_uv


def updateNumPoints(self, context):
    old_num_items = len(self.inputs) - 2
    if old_num_items < self.num_items:
        for x in range(old_num_items, self.num_items):
            self.inputs.new( 'CoronaPoint', "Point %d" % (x+1))
        old_num_items = self.num_items
    if old_num_items > self.num_items:
        for x in range(old_num_items, self.num_items, -1):
            self.inputs.remove( self.inputs["Point %d" % x])
        old_num_items = self.num_items
    return None
#--------------------------------
# Curve shader node.
#--------------------------------
class CoronaCurveNode( Node, CoronaNode):
    '''Corona Curve Shader Node'''
    bl_idname = "CoronaCurveNode"
    bl_label = "Curve Shader"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of points', default=2, min=2, max=10, update=updateNumPoints)

    # curve_data = bpy.props.StringProperty( name = 'Curve data')

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaPoint', "Point 1")
        self.inputs.new( 'CoronaPoint', "Point 2")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons(self, context, layout):
        layout.prop(self, 'num_items', 'Points')

        # layout.template_curve_mapping( self, 'curve_data', type='VECTOR', levels=False, brush=False, use_negative_slope=False )

    def get_node_params( self, root, inline = False):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Curve')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+1):
                xml_mat.append(self.inputs["Point %d" % x].get_socket_value(self, root))
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Interpolation shader node.
#--------------------------------
class CoronaInterpolationNode( Node, CoronaNode):
    '''Corona Interpolation Node'''
    bl_idname = "CoronaInterpolationNode"
    bl_label = "Interpolation Shader"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of points', default=2, min=2, max=10, update=updateNumPoints)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaPoint', "Point 1")
        self.inputs.new( 'CoronaPoint', "Point 2")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons(self, context, layout):
        layout.prop(self, 'num_items', 'Points')

    def get_node_params( self, root, inline = False):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Interpolation')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+1):
                xml_mat.append(self.inputs["Point %d" % x].get_socket_value(self, root))
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Normal shader node.
#--------------------------------
class CoronaNormalNode( Node, CoronaNode):
    '''Corona Normal Shader Node'''
    bl_idname = "CoronaNormalNode"
    bl_label = "Normal Shader"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketShader', "Color")

    def get_node_params( self, root, inline = False):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Normal')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Child", "child", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# FrontBack shader node.
#--------------------------------
class CoronaFrontBackNode( Node, CoronaNode):
    '''Corona FrontBack Shader Node'''
    bl_idname = "CoronaFrontBackNode"
    bl_label = "FrontBack Shader"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Front")
        self.inputs.new( 'CoronaColor', "Back")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketShader', "Color")

    def get_node_params( self, root, inline = False):

        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'FrontBack')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Front", "front", inline)
            addSocket(root, xml_mat, self, "Back", "back", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

def getChannelEnum(default, name):
    return bpy.props.EnumProperty( name = name,
                                description = "Select source for this channel",
                                default = default,
                                items = [
                                    ('R', 'R', 'Red channel of the input'),
                                    ('G', 'G', 'Green channel of the input'),
                                    ('B', 'B', 'Blue channel of the input'),
                                    ('A', 'A', 'Alpha channel of the input'),
                                    ('RgbIntensity', 'RGB Intensity', 'Average of Red, Green and Blue of the input'),
                                    ('One', 'One', 'Output channel is set to the value of 1.0')])

#--------------------------------
# Channel shader node.
#--------------------------------
class CoronaChannelNode( Node, CoronaNode):
    '''Corona Channel Shader Node'''
    bl_idname = "CoronaChannelNode"
    bl_label = "Channel Shader"
    bl_icon = 'SMOOTH'

    rsource = getChannelEnum('R', 'Red Source')
    gsource = getChannelEnum('G', 'Green Source')
    bsource = getChannelEnum('B', 'Blue Source')
    asource = getChannelEnum('A', 'Alpha Source')
    monosource = getChannelEnum('RgbIntensity', 'Mono Source')

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaLinked', "UV Map")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'rsource')
        layout.prop(self, 'gsource')
        layout.prop(self, 'bsource')
        layout.prop(self, 'asource')
        layout.prop(self, 'monosource')

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Channel')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Child", "child", inline)
            SubElement(xml_mat, 'rSource').text = self.rsource
            SubElement(xml_mat, 'gSource').text = self.gsource
            SubElement(xml_mat, 'bSource').text = self.bsource
            SubElement(xml_mat, 'alphaSource').text = self.asource
            SubElement(xml_mat, 'monoSource').text = self.monosource
            addSocket(root, xml_mat, self, "UV Map", "uvMap", inline)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Tone Map shader node.
#--------------------------------
class CoronaToneMapNode( Node, CoronaNode):
    '''Corona Normal Shader Node'''
    bl_idname = "CoronaToneMapNode"
    bl_label = "Tone Map Shader"
    bl_icon = 'SMOOTH'

    invert = bpy.props.BoolProperty(name = 'Invert', description = 'output = 1 - output', default = False)
    clamp = bpy.props.BoolProperty(name = 'Clamp', description = 'output = clamp(output, 0, 1)', default = False)
    absolute = bpy.props.BoolProperty(name = 'Absolute', description = 'output = abs(output)', default = False)
    remove = bpy.props.BoolProperty(name = 'Remove Tone Mapping', description = 'output = inverseImageToneMapping(output)', default = False)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaMultiply', "Multiplier")
        self.inputs.new( 'CoronaFloat', "Alpha Multiplier")
        self.inputs.new( 'CoronaColor', "Offset")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'invert')
        layout.prop(self, 'clamp')
        layout.prop(self, 'absolute')
        layout.prop(self, 'remove')

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'ToneMap')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Child", "child", inline)
            addSocket(root, xml_mat, self, "Multiplier", "multiplier", inline)
            addSocket(root, xml_mat, self, "Alpha Multiplier", "alphaMultiplier", inline)
            addSocket(root, xml_mat, self, "Offset", "offset", inline)
            SubElement(xml_mat, 'invert').text = 'True' if self.invert else 'False'
            SubElement(xml_mat, 'clamp').text = 'True' if self.clamp else 'False'
            SubElement(xml_mat, 'abs').text = 'True' if self.absolute else 'False'
            SubElement(xml_mat, 'removeToneMapping').text = 'True' if self.remove else 'False'
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

def updateNumItems(self, context):
    old_num_items = len(self.inputs) - 4
    if old_num_items < self.num_items:
        for x in range(old_num_items, self.num_items):
            self.inputs.new( 'CoronaColor', "Child %d" % (x+1))
        old_num_items = self.num_items
    if old_num_items > self.num_items:
        for x in range(old_num_items, self.num_items, -1):
            self.inputs.remove( self.inputs["Child %d" % x])
        old_num_items = self.num_items
    return None

#--------------------------------
# Select shader node.
#--------------------------------
class CoronaSelectNode( Node, CoronaNode):
    '''Corona Select Shader Node'''
    bl_idname = "CoronaSelectNode"
    bl_label = "Select Shader"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of submaps', default=1, min=0, max=10, update=updateNumItems)
    mode = bpy.props.EnumProperty(name = 'Mode', default  = 'Material', items = [
        ('Material', 'Material', ''),
        ('MaterialId', 'Material Id', ''),
        ('Instance', 'Instance', ''),
        ('Primitive', 'Primitive', '')
        ])

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Multiplier")
        self.inputs.new( 'CoronaFloat010', "Mix Min")
        self.inputs.new( 'CoronaFloat011', "Mix Max")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.inputs.new( 'CoronaColor', "Child 1")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'mode')
        layout.prop(self, 'num_items')

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Select')

        if xml_mat != None:
            SubElement(xml_mat, 'mode').text = self.mode
            addSocket(root, xml_mat, self, "Mix Min", "mixMin", inline)
            addSocket(root, xml_mat, self, "Mix Max", "mixMax", inline)
            for x in range(1, self.num_items+1):
                addSocket(root, xml_mat, self, "Child %d" % x, "item", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Mix shader node.
#--------------------------------
class CoronaMixNode( Node, CoronaNode):
    '''Corona Mix Shader Node'''
    bl_idname = "CoronaMixNode"
    bl_label = "Mix Shader"
    bl_icon = 'SMOOTH'

    mix_type = bpy.props.EnumProperty( name = "Operation",
                                        description = "Operation to perform on the two color inputs",
                                        default = 'mix',
                                        items = [
                                            ('mix', 'Mix', 'PBR Mix the two nodes = (a * (1 - amount)) + (b * amount)'),
                                            ('add', 'Add', 'Add the two nodes (possibly not PBR) = a + b'),
                                            ('mul', 'Multiply', 'Multiply the two nodes (possibly not PBR) = a * b'),
                                            ('sub', 'Subtract', 'Subtract = a - b')])

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Color A")
        self.inputs.new( 'CoronaColor', "Color B")
        self.inputs.new( 'CoronaMixAmount', "Amount")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'mix_type')

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Mix')

        if xml_mat != None:
            SubElement(xml_mat, 'operation').text = self.mix_type

            addSocket(root, xml_mat, self, "Color A", "a", inline)
            addSocket(root, xml_mat, self, "Color B", "b", inline)
            addSocket(root, xml_mat, self, "Amount", "amountB", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Fresnel shader node.
#--------------------------------
class CoronaFresnelNode( Node, CoronaNode):
    '''Corona Fresnel Shader Node'''
    bl_idname = "CoronaFresnelNode"
    bl_label = "Fresnel Shader"
    bl_icon = 'SMOOTH'

    ior = bpy.props.FloatProperty( name = "IOR",
                                    description = "Applies a fresnel curve on mono input provided by a child node (higher IOR = more prominent child color)",
                                    default = 1.333,
                                    min = 1,
                                    max = 10)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Child")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "ior")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        debug("Outputting fresnel", name_pointer)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Fresnel')

        if xml_mat != None:
            SubElement(xml_mat, 'ior').text = '%.4f' % self.ior
            addSocket(root, xml_mat, self, "Child", "child", inline)
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaWireframeNode( Node, CoronaNode):
    '''Corona Wireframe Shader Node'''
    bl_idname = "CoronaWireframeNode"
    bl_label = "Wireframe Shader"
    bl_icon = 'SMOOTH'

    edge_width = bpy.props.FloatProperty( name = "Edge Width",
                                    description = "",
                                    default = 1,
                                    min = 0,
                                    soft_max = 10,
                                    unit = 'LENGTH')

    vertex_width = bpy.props.FloatProperty( name = "Vertex Width",
                                    description = "",
                                    default = 1,
                                    min = 0,
                                    soft_max = 10,
                                    unit = 'LENGTH')

    world_space = bpy.props.BoolProperty( name = "World Space",
                                    description = "If true, then edgeWidth and VertexWidth are in world units. If false, they are in screen units (projected pixels)",
                                    default = False)

    all_edges = bpy.props.BoolProperty( name = "All Edges",
                                    description = "If false then only the base mesh edges are visualized. Base mesh edges are those the user see in Blender. All edges created by Corona (for example in displacement) remain hidden",
                                    default = False)

    def init( self, context):
        self.inputs.new( 'CoronaColor', "Base")
        self.inputs.new( 'CoronaLinkedMap', "Edge")
        self.inputs.new( 'CoronaLinkedMap', "Vertex")
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "edge_width")
        layout.prop( self, "vertex_width")
        layout.prop( self, "world_space")
        layout.prop( self, "all_edges")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Wire')

        if xml_mat != None:
            SubElement(xml_mat, 'edgeWidth').text = '%.4f' % self.edge_width
            SubElement(xml_mat, 'vertexWidth').text = '%.4f' % self.vertex_width
            SubElement(xml_mat, 'worldSpace').text = 'True' if self.world_space else 'False'
            SubElement(xml_mat, 'allEdges').text = 'True' if self.all_edges else 'False'

            addSocket(root, xml_mat, self, "Base", "base", inline)
            addSocket(root, xml_mat, self, "Edge", "edge", inline)
            addSocket(root, xml_mat, self, "Vertex", "vertex", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)


class CoronaCheckerNode( Node, CoronaNode):
    bl_idname = "CoronaCheckerNode"
    bl_label = "Checker Pattern"
    bl_icon = 'SMOOTH'

    size  = bpy.props.FloatProperty( name = "Scale",
                                description = "Scale of pattern",
                                default = 1.0,
                                subtype = "NONE",
                                soft_min = 0.1)

    def init( self, context):
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "size")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Checker')
        if xml_mat != None:
            SubElement(xml_mat, 'size').text = '%.3f' % self.size
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaNoiseNode( Node, CoronaNode):
    bl_idname = "CoronaNoiseNode"
    bl_label = "Noise Generator"
    bl_icon = 'SMOOTH'

    type = bpy.props.EnumProperty( name = "Type",
                                        description = "Noise Type",
                                        default = 'perlin',
                                        items = [
                                            ('perlin', 'Perlin', ''),
                                            ('cellular', 'Cellular', '')])

    size  = bpy.props.FloatProperty( name = "Scale",
                                description = "Scale of pattern",
                                default = 1.0,
                                subtype = "NONE",
                                soft_min = 0.1)

    levels  = bpy.props.FloatProperty( name = "Levels",
                                description = "",
                                default = 1.0,
                                subtype = "NONE")

    phase  = bpy.props.FloatProperty( name = "Phase",
                                description = "",
                                default = 0.0,
                                subtype = "NONE")

    def init( self, context):
        self.inputs.new( 'CoronaLinked', 'uvm')
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "type")
        layout.prop( self, "size")
        layout.prop( self, "levels")
        layout.prop( self, "phase")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Noise')
        if xml_mat != None:
            SubElement(xml_mat, 'type').text = self.type
            SubElement(xml_mat, 'size').text = '%.3f' % self.size
            SubElement(xml_mat, 'levels').text = '%.3f' % self.levels
            SubElement(xml_mat, 'phase').text = '%.3f' % self.phase
            addChild(root, xml_mat, self, "uvm", True)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Solid colour node.
#--------------------------------
class CoronaSolidNode( Node, CoronaNode):
    '''Corona Fresnel Shader Node'''
    bl_idname = "CoronaSolidNode"
    bl_label = "Solid Color"
    bl_icon = 'SMOOTH'

    color  = bpy.props.FloatVectorProperty( name = "Color",
                                description = "Color optimised so there is no performance penalty for using",
                                default = (1.0, 1.0, 1.0, 1.0),
                                subtype = "COLOR",
                                size = 4,
                                min = 0.0,
                                max = 1.0)

    def init( self, context):
        self.outputs.new( 'NodeSocketShader', "Color")

    def draw_buttons( self, context, layout):
        layout.prop( self, "color")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Solid')
        if xml_mat != None:
            xml_mat.text = '%.3f %.3f %.3f %.3f' % self.color[:]
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

#--------------------------------
# Ray switch shader node.
#--------------------------------
class CoronaRaySwitchNode( Node, CoronaNode):
    bl_idname = "CoronaRaySwitchNode"
    bl_label = "Ray Switch Shader"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaColor', "GI")
        self.inputs.new( 'CoronaLinkedMap', "Reflect")
        self.inputs.new( 'CoronaLinkedMap', "Refract")
        self.inputs.new( 'CoronaLinkedMap', "Direct")
        self.outputs.new( 'NodeSocketShader', "Color")

    def get_node_params( self, root, inline = False):
        ''' Return the Ray Switch shader node's parameters as a tuple (list, string) '''
        name_pointer = join_names_underscore( self.name, str(self.as_pointer()))

        xml_mat, xml_mat_def = getMap(root, name_pointer, 'RaySwitcher')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "GI", "base", inline)
            addSocket(root, xml_mat, self, "Reflect", "reflect", inline)
            addSocket(root, xml_mat, self, "Refract", "refract", inline)
            addSocket(root, xml_mat, self, "Direct", "direct", inline)
            if not inline: root.append(xml_mat_def)
        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

class CoronaOutputNode( Node, CoronaNode):
    '''Corona Output Node'''
    bl_idname = "CoronaOutputNode"
    bl_label = "Corona Output"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMaterial', "Material")

    def get_node_params( self, root, inline = False):
        socket = self.inputs['Material']
        if socket.is_linked:
            return socket.get_socket_params(self, root, inline)


class CoronaShadowcatcherOutputNode( Node, CoronaNode):
    '''Corona Shadow Catcher Output Node'''
    bl_idname = "CoronaShadowcatcherOutputNode"
    bl_label = "Corona Shadowcatcher"
    bl_icon = 'SMOOTH'

    mode = bpy.props.EnumProperty( name = "Shading Mode",
                                        description = "A special fake for compositing. It has a limited subset of the native material, plus some extra options that allows using it for compositing rendered imagery on top of live footage.",
                                        default = 'final',
                                        items = [
                                            ('final', 'Final', 'Final means that both the photo background (defined in emission slot) and the CG footage will be present in the resulting render'),
                                            ('composite', 'Composite', 'Composite means that only the CG footage will be present (but the emission slot will still be used to create shadows/alpha for compositing)'),
                                            ('finalNoAlpha', 'Final No Alpha', '')
                                            ])

    projmode = bpy.props.EnumProperty( name = "Projection Mode",
                                        description = "A special fake for compositing. It has a limited subset of the native material, plus some extra options that allows using it for compositing rendered imagery on top of live footage.",
                                        default = 'none',
                                        items = [
                                            ('none', 'None', 'Textures will be used as-is'),
                                            ('backplate', 'Backplate', 'Textures will be reprojected to create contact shadows (to make the CG footage “touch” the background)'),
                                            ('environment', 'Environment', 'Textures will be reprojected to create contact shadows (to make the CG footage “touch” the background)')
                                            ])

    projAllMaps = bpy.props.BoolProperty( name = "Project All Maps",
                                    description = "The projectionMode will be used also for bump/reflections maps (not only emission)",
                                    default = True)

    shadowAmount = bpy.props.FloatProperty( name = "Shadow Amount",
                                         description = "Modifies the amount of shadows in the picture by changing the albedo of the shadow catcher",
                                         min = 0,
                                         default = 0)

    def init( self, context):
        self.inputs.new( 'CoronaColor', 'Emission')
        self.inputs.new( 'CoronaLinked', 'Offscreen Override')
        self.inputs.new( 'CoronaLinked', "Reflect Color")
        self.inputs.new( 'CoronaReflectGloss', "Reflect Gloss")
        self.inputs.new( 'CoronaReflectFresnel', "Reflect Fresnel")
        self.inputs.new( 'CoronaLinked', "Bump")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons(self, context, layout):
        layout.prop(self, "mode", "Shading Mode")
        layout.prop(self, "projmode", "Projection Mode")
        layout.prop(self, "projAllMaps", "Project All Maps")
        layout.prop(self, "shadowAmount", "Shadow Amount")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'ShadowCatcher')
        if xml_mat != None:

            SubElement(xml_mat, 'shadingMode').text = self.mode
            SubElement(xml_mat, 'projectionMode').text = self.projmode
            SubElement(xml_mat, 'projectAllMaps').text = 'True' if self.projAllMaps else 'False'
            SubElement(xml_mat, 'shadowAmount').text = '%.3f' % self.shadowAmount

            addSocket(root, xml_mat, self, "Offscreen Override", "offscreenOverride", inline)
            addSocket(root, xml_mat, self, "Emission", "emission", inline)
            addSocket(root, xml_mat, self, "Bump", "bump", inline)
            if self.inputs["Reflect Color"].is_linked:
                xml_reflect = SubElement(xml_mat, "reflect")
                addSocket(root, xml_reflect, self, "Reflect Color", "color", inline)
                addSocket(root, xml_reflect, self, "Reflect Gloss", "glossiness", inline)
                addSocket(root, xml_reflect, self, "Reflect Fresnel", "ior", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

class CoronaRaySwitchOutputNode( Node, CoronaNode):
    '''Corona RaySwitch Output Node'''
    bl_idname = "CoronaRaySwitchOutputNode"
    bl_label = "Corona RaySwitch Material"
    bl_icon = 'SMOOTH'

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMaterial', "Normal")
        self.inputs.new( 'CoronaLinkedMaterial', "Reflect")
        self.inputs.new( 'CoronaLinkedMaterial', "Refract")
        self.inputs.new( 'CoronaLinkedMaterial', "Direct")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'Rayswitcher')
        if xml_mat != None:
            addSocket(root, xml_mat, self, "Normal", "normal", inline)
            addSocket(root, xml_mat, self, "Reflect", "reflect", inline)
            addSocket(root, xml_mat, self, "Refract", "refract", inline)
            addSocket(root, xml_mat, self, "Direct", "direct", inline)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

def updateNumLayers(self, context):
    old_num_items = math.floor((len(self.inputs) - 1) / 2)
    if old_num_items < self.num_items:
        for x in range(old_num_items, self.num_items):
            self.inputs.new( 'CoronaLinkedMaterial', "Layer %d" % (x+1))
            self.inputs.new( 'CoronaLayeredWeight', "Weight %d" % (x+1))
        old_num_items = self.num_items
    if old_num_items > self.num_items:
        for x in range(old_num_items, self.num_items, -1):
            self.inputs.remove( self.inputs["Layer %d" % x])
            self.inputs.remove( self.inputs["Weight %d" % x])
        old_num_items = self.num_items
    return None

class CoronaLayeredOutputNode( Node, CoronaNode):
    '''Corona Layered Output Node'''
    bl_idname = "CoronaLayeredOutputNode"
    bl_label = "Corona Layered Material"
    bl_icon = 'SMOOTH'

    num_items = bpy.props.IntProperty(name = 'Number of layers', default=1, min=1, max=10, update=updateNumLayers)

    def init( self, context):
        self.inputs.new( 'CoronaLinkedMaterial', "Base")
        self.inputs.new( 'CoronaLinkedMaterial', "Layer 1")
        self.inputs.new( 'CoronaLayeredWeight', "Weight 1")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        layout.prop(self, 'num_items')

    def get_node_params( self, root, inline = False):
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer, 'Layered')
        if xml_mat != None:
            addSocket(root, xml_mat, self, "Base", "base", inline)
            for x in range(1, self.num_items+1, 1):
                xml_layer = addSocket(root, xml_mat, self, "Layer %d" % x, "layer", inline)
                xml_layer = addSocket(root, xml_layer, self, "Weight %d" % x, "weight", inline)

            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

class RENDER_UL_Object_slots(bpy.types.UIList):
    def draw_item( self, context, layout, data, item, icon, active_data, active_propname, index):

        if 'DEFAULT' in self.layout_type:
            layout.label( text = item.name, translate=False, icon_value=icon)

class CoronaObjectListNode( Node, CoronaNode):
    '''Corona Object List Node'''
    bl_idname = "CoronaObjectListNode"
    bl_label = "Scene Objects"
    bl_icon = 'SMOOTH'

    includes = bpy.props.CollectionProperty(name = "Included Objects", type = CoronaObjects)

    include_index = bpy.props.IntProperty( name = "Include Index",
                                        description = "",
                                        default = 0,
                                        min = 0,
                                        max = 100)

    def init( self, context):
        self.outputs.new("CoronaObjectList", "Output")

    def draw_buttons( self, context, layout):
        row = layout.row()
        row.template_list( "RENDER_UL_Object_slots", "corona_object_list", self, "includes", self, "include_index")

        row = layout.row(align=True)
        op = row.operator( "corona.add_object", icon = "ZOOMIN")
        op.material = self.name
        op = row.operator( "corona.remove_object", icon = "ZOOMOUT")
        op.material = self.name
        op.index = self.include_index

    def get_node_params( self, root, inline = False):
        return self.includes

#--------------------------------
# Material output node.
#--------------------------------

def updateInputs(self, context):
    self.inputs["Diffuse Level"].hide = not self.use_diffuse or self.as_portal
    self.inputs["Diffuse Color"].hide = not self.use_diffuse or self.as_portal
    self.inputs["Translucency Level"].hide = not self.use_translucency or self.as_portal
    self.inputs["Translucency Color"].hide = not self.use_translucency or self.as_portal
    self.inputs["Reflect Level"].hide = not self.use_reflect or self.as_portal
    self.inputs["Reflect Color"].hide = not self.use_reflect or self.as_portal
    self.inputs["Reflect Gloss"].hide = not self.use_reflect or self.as_portal
    self.inputs["Fresnel"].hide = not self.use_reflect or self.as_portal
    self.inputs["Anisotropy"].hide = not self.use_reflect or self.as_portal
    self.inputs["Anisotropy Rotation"].hide = not self.use_reflect or self.as_portal
    self.inputs["Refract Level"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract Color"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract Gloss"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract IOR"].hide = not self.use_refract or self.as_portal
    self.inputs["Refract Mode"].hide = not self.use_refract or self.as_portal
    self.inputs["Absorption Color"].hide = not self.use_absorption or self.as_portal
    self.inputs["Absorption Distance"].hide = not self.use_absorption or self.as_portal
    self.inputs["Scattering Color"].hide = not self.use_scattering or self.as_portal
    self.inputs["Scattering Direction"].hide = not self.use_scattering or self.as_portal
    self.inputs["Opacity"].hide = not self.use_opacity and not self.as_portal
    self.inputs["Bump"].hide = not self.use_bump or self.as_portal
    self.inputs["Displacement"].hide = not self.use_displace or self.as_portal
    self.inputs["Emission Color"].hide = not self.use_emit or self.as_portal
    self.inputs["Directionality"].hide = not self.use_emit or self.as_portal
    self.inputs["Multiplier"].hide = not self.use_emit or self.as_portal
    self.inputs["Include"].hide = not self.use_emit or self.as_portal
    self.inputs["Exclude"].hide = not self.use_emit or self.as_portal
    return None

class CoronaMtlNode( Node, CoronaNode):
    '''Corona Material Output Node'''
    bl_idname = "CoronaMtlNode"
    bl_label = "Corona Material"
    bl_icon = 'SMOOTH'

    params = []

    # as_portal       = CoronaMatProps.as_portal

    as_portal = bpy.props.BoolProperty( name = "Use material as light portal",
                                description = "Material will be used as a light portal. Portal material will not be visible to the camera",
                                default = False,
                                update = updateInputs)

    self_illumination = bpy.props.BoolProperty( name = "Self Illumination",
                                    description = "When true it disables sampling and provides self illumination only",
                                    default = False)

    disp_max = bpy.props.FloatProperty( name = "Max Displacement Value",
                                    default = 1.0,
                                    soft_min = 0.0,
                                    soft_max = 10.0)
    disp_min = bpy.props.FloatProperty( name = "Min Displacement Value",
                                    default = 0.0,
                                    soft_min = 0.0,
                                    soft_max = 10.0)
    disp_water = bpy.props.FloatProperty( name = "Displacement Water Level",
                                    default = 0.5,
                                    soft_min = 0.0,
                                    soft_max = 10.0)

    use_configure = bpy.props.BoolProperty(name = "Configure",
                                    description = "Configure the visible properties for this material (Connected nodes can't be hidden and the hidden values still take effect)",
                                    default = False)
    use_diffuse = bpy.props.BoolProperty(name = "Diffuse",
                                    description = "Show diffuse inputs",
                                    default = True,
                                    update=updateInputs)
    use_translucency = bpy.props.BoolProperty(name = "Translucency",
                                    description = "Show translucency inputs",
                                    default = True,
                                    update=updateInputs)
    use_reflect = bpy.props.BoolProperty(name = "Reflection",
                                    description = "Show reflection inputs",
                                    default = True,
                                    update=updateInputs)
    use_refract = bpy.props.BoolProperty(name = "Refract",
                                    description = "Show refraction inputs",
                                    default = True,
                                    update=updateInputs)
    use_absorption = bpy.props.BoolProperty(name = "Absorption",
                                    description = "Show absorption inputs",
                                    default = True,
                                    update=updateInputs)
    use_scattering = bpy.props.BoolProperty(name = "Scattering",
                                    description = "Show scattering inputs",
                                    default = True,
                                    update=updateInputs)
    use_bump = bpy.props.BoolProperty(name = "Bump",
                                    description = "Show bump input",
                                    default = True,
                                    update=updateInputs)
    use_displace = bpy.props.BoolProperty(name = "Displacement",
                                    description = "Show displacement input",
                                    default = True,
                                    update=updateInputs)
    use_opacity = bpy.props.BoolProperty(name = "Opacity",
                                    description = "Show opacity input",
                                    default = True,
                                    update=updateInputs)
    use_emit = bpy.props.BoolProperty(name = "Emit",
                                    description = "Show emission inputs",
                                    default = True,
                                    update=updateInputs)

    def init( self, context):
        self.inputs.new( 'CoronaDiffuseLevel', "Diffuse Level")
        self.inputs.new( 'CoronaKd', "Diffuse Color")
        self.inputs.new( 'CoronaTranslucencyLevel', "Translucency Level")
        self.inputs.new( 'CoronaTranslucency', "Translucency Color")
        self.inputs.new( 'CoronaReflectLevel', "Reflect Level")
        self.inputs.new( 'CoronaKs', "Reflect Color")
        self.inputs.new( 'CoronaReflectGloss', "Reflect Gloss")
        self.inputs.new( 'CoronaReflectFresnel', "Fresnel")
        self.inputs.new( 'CoronaAnisotropy', "Anisotropy")
        self.inputs.new( 'CoronaAnisotropyRot', "Anisotropy Rotation")
        self.inputs.new( 'CoronaRefractLevel', "Refract Level")
        self.inputs.new( 'CoronaRefractColor', "Refract Color")
        self.inputs.new( 'CoronaRefractGloss', "Refract Gloss")
        self.inputs.new( 'CoronaRefractIOR', "Refract IOR")
        self.inputs.new( 'CoronaRefractMode', "Refract Mode")
        self.inputs.new( 'CoronaAbsorptionColor', "Absorption Color")
        self.inputs.new( 'CoronaAbsorptionDist', "Absorption Distance")
        self.inputs.new( 'CoronaScattering', "Scattering Color")
        self.inputs.new( 'CoronaMeanCosine', "Scattering Direction")
        self.inputs.new( 'CoronaOpacity', "Opacity")
        # self.inputs.new( 'CoronaNormal', "Normal")
        self.inputs.new( 'CoronaBump', "Bump")
        # self.inputs.new( 'CoronaBump', "Displacement")
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMultZero', "Multiplier")
        self.inputs.new( 'CoronaObjectList', "Include")
        self.inputs.new( 'CoronaObjectList', "Exclude")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):

        layout.prop( self, "as_portal")
        if not self.as_portal:
            layout.prop( self, "use_configure")
        if self.use_configure and not self.as_portal:
            row = layout.row()
            row.prop( self, "use_diffuse")
            row.prop( self, "use_translucency")
            row.prop( self, "use_reflect")
            row = layout.row()
            row.prop( self, "use_refract")
            row.prop( self, "use_absorption")
            row.prop( self, "use_scattering")
            row = layout.row()
            row.prop( self, "use_opacity")
            row.prop( self, "use_bump")
            row.prop( self, "use_displace")
            row = layout.row()
            row.prop( self, "use_emit")
        if self.use_emit and not self.as_portal:
            layout.prop( self, "self_illumination")
        if self.use_displace and not self.as_portal:
            layout.prop( self, "disp_min")
            layout.prop( self, "disp_max")
            layout.prop( self, "disp_water")

    def get_node_params( self, root, inline = False):
        ''' Return the Material node's parameters as a string '''
        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer)

        if xml_mat != None:

            if not self.as_portal:
                addSocket(root, xml_mat, self, "Diffuse Color", "diffuse", inline)

                xml_trans = SubElement(xml_mat, 'translucency')
                addSocket(root, xml_trans, self, "Translucency Level", "level", inline)
                addSocket(root, xml_trans, self, "Translucency Color", "color", inline)

                xml_reflect = SubElement(xml_mat, 'reflect')
                addSocket(root, xml_reflect, self, "Reflect Color", "color", inline)
                addSocket(root, xml_reflect, self, "Reflect Gloss", "glossiness", inline)
                addSocket(root, xml_reflect, self, "Fresnel", "ior", inline)
                addSocket(root, xml_reflect, self, "Anisotropy", "anisotropy", inline)
                addSocket(root, xml_reflect, self, "Anisotropy Rotation", "anisoRotation", inline)
                # TODO add extra anisotropy parameters

                xml_refract = SubElement(xml_mat, 'refract')
                addSocket(root, xml_refract, self, "Refract Color", "color", inline)
                addSocket(root, xml_refract, self, "Refract IOR", "ior", inline)
                addSocket(root, xml_refract, self, "Refract Gloss", "glossiness", inline)
                addSocket(root, xml_refract, self, "Refract Mode", "glassMode", inline)

                xml_vol = SubElement(xml_mat, 'volume')
                addSocket(root, xml_vol, self, "Absorption Color", "attenuationColor", inline)
                addSocket(root, xml_vol, self, "Absorption Distance", "attenuationDist", inline)
                addSocket(root, xml_vol, self, "Scattering Color", "scatteringAlbedo", inline)
                addSocket(root, xml_vol, self, "Scattering Direction", "meanCosine", inline)

                if self.inputs["Bump"].is_linked:
                    addSocket(root, xml_mat, self, "Bump", "bump", inline)

                '''
                if self.inputs["Displacement"].is_linked:
                    xml_disp = SubElement(xml_mat, 'displacement')
                    xml_map = addSocket(root, xml_disp, self, "Displacement", "map", True)
                    xml_map.set('class', 'Displacement')
                    # xml_map = SubElement(xml_disp, 'map').text = '0.5 0.5 0.5'
                    # addSocket(root, xml_map, self, "Displacement", inline)
                    SubElement(xml_disp, 'max').text = '%.3f' % self.disp_max
                    SubElement(xml_disp, 'min').text = '%.3f' % self.disp_min
                    SubElement(xml_disp, 'waterLevel').text = '%.3f' % self.disp_water
                '''

                addSocket(root, xml_mat, self, "Opacity", "opacity", inline)

                xml_emit = SubElement(xml_mat, 'emission')
                addSocket(root, xml_emit, self, "Emission Color", "color", inline)
                addSocket(root, xml_emit, self, "Directionality", "glossiness", inline)
                SubElement(xml_emit, 'disableSampling').text = 'True' if self.self_illumination else 'False'

                if self.inputs["Include"].is_linked or self.inputs["Exclude"].is_linked:
                    xml_inex = SubElement(xml_emit, 'includeExclude')
                    if self.inputs["Include"].is_linked:
                        includes = self.inputs["Include"].get_socket_params(self, root, inline)
                        for inc in includes:
                            SubElement(xml_inex, 'included').text = inc.name

                    if self.inputs["Exclude"].is_linked:
                        excludes = self.inputs["Exclude"].get_socket_params(self, root, inline)
                        for excl in excludes:
                            SubElement(xml_inex, 'excluded').text = excl.name
            else: # as_portal
                addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
                SubElement(xml_mat, 'portal').text = 'true' if self.as_portal else 'false'

            if not inline: root.append(xml_mat_def)


        # Return material reference
        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

#--------------------------------
# Light material output node.
#--------------------------------
class CoronaLightMtlNode( Node, CoronaNode):
    '''Corona Light Material Output Node'''
    bl_idname = "CoronaLightMtlNode"
    bl_label = "Corona Light Material"
    bl_icon = 'SMOOTH'

    ies_profile     = CoronaMatProps.ies_profile
    keep_sharp      = CoronaMatProps.keep_sharp
    ies_translate   = CoronaMatProps.ies_translate
    ies_rotate      = CoronaMatProps.ies_rotate
    ies_scale       = CoronaMatProps.ies_scale

    self_illumination = bpy.props.BoolProperty( name = "Self Illumination",
                                    description = "When true it disables sampling and provides self illumination only",
                                    default = False)

    def init( self, context):
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMult', "Multiplier")
        self.inputs.new( 'CoronaOpacity', "Opacity")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        img = bpy.data.images.get('CORONA32')
        if img is not None:
            icon = layout.icon(img)
            layout.label(text="", icon_value=icon)
        col = layout.column()
        col.prop( self, "ies_profile")
        col.prop( self, "keep_sharp")
        col.prop( self, "ies_translate")
        col.prop( self, "ies_rotate")
        col.prop( self, "ies_scale")
        col.prop( self, "self_illumination")

    def get_node_params( self, root, inline = False):
        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer)

        if xml_mat != None:
            xml_emit = SubElement(xml_mat, 'emission')

            addSocket(root, xml_emit, self, "Emission Color", "color", inline)
            addSocket(root, xml_emit, self, "Directionality", "glossiness", inline)
            SubElement(xml_emit, 'disableSampling').text = 'True' if self.self_illumination else 'False'

            if self.ies_profile != '':

                mat = (mathutils.Matrix.Translation(self.ies_translate)
                    * mathutils.Matrix.Rotation(math.radians(self.ies_rotate[0]), 4, 'X')
                    * mathutils.Matrix.Rotation(math.radians(self.ies_rotate[1]), 4, 'Y')
                    * mathutils.Matrix.Rotation(math.radians(self.ies_rotate[2]), 4, 'Z')
                    * mathutils.Matrix.Scale(self.ies_scale[0], 4, (1.0, 0.0, 0.0))
                    * mathutils.Matrix.Scale(self.ies_scale[1], 4, (0.0, 1.0, 0.0))
                    * mathutils.Matrix.Scale(self.ies_scale[2], 4, (0.0, 0.0, 1.0))
                    );

                matrix = ' '.join([str(i) for m in mat for i in m])
                debug('iesm', matrix)
                xml_ies = SubElement(xml_emit, 'ies')
                SubElement(xml_ies, 'file').text = realpath( self.ies_profile)
                SubElement(xml_ies, 'tm').text = matrix
                SubElement(xml_ies, 'sharpnessFake').text = 'true' if self.keep_sharp else 'false'

            addSocket(root, xml_mat, self, "Opacity", "opacity", inline)
            if not inline: root.append(xml_mat_def)


        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)


#--------------------------------
# Volume material output node.
#--------------------------------
class CoronaVolumeMtlNode( Node, CoronaNode):
    '''Corona Volume Material Output Node'''
    bl_idname = "CoronaVolumeMtlNode"
    bl_label = "Corona Volume Material"
    bl_icon = 'SMOOTH'

    params = []

    def init( self, context):
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMult', "Multiplier")
        self.inputs.new( 'CoronaAbsorptionColor', "Absorption Color")
        self.inputs.new( 'CoronaAbsorptionDist', "Absorption Distance")
        self.inputs.new( 'CoronaScattering', "Scattering Color")
        self.inputs.new( 'CoronaMeanCosine', "Scattering Direction")
        self.outputs.new( 'CoronaLinkedMaterial', "Material")

    def draw_buttons( self, context, layout):
        img = bpy.data.images.get('CORONA32')
        if img is not None:
            icon = layout.icon(img)
            layout.label(text="", icon_value=icon)

    def get_node_params( self, root, inline = False):
        ''' Return the Material node's parameters as a string '''

        # Add material definition
        name_pointer = getNodePtr(self)

        xml_mat, xml_mat_def = getMat(root, name_pointer)

        if xml_mat != None:
            xml_vol = SubElement(xml_mat, 'volume')

            addSocket(root, xml_vol, self, "Absorption Color", "attenuationColor", inline)
            addSocket(root, xml_vol, self, "Absorption Distance", "attenuationDist", inline)
            addSocket(root, xml_vol, self, "Emission Color", "emissionColor", inline)
            addSocket(root, xml_vol, self, "Directionality", "emissionDist", inline)
            addSocket(root, xml_vol, self, "Scattering Color", "scatteringAlbedo", inline)
            addSocket(root, xml_vol, self, "Scattering Direction", "meanCosine", inline)

            # xml_refract = SubElement(xml_mat, 'refract')
            # SubElement(xml_refract, 'glassMode').text = 'caustics'
            SubElement(xml_mat, 'opacity').text = '0 0 0'
            if not inline: root.append(xml_mat_def)


        if inline:
            return xml_mat
        else:
            return getReference(name_pointer)

#--------------------------------
# Node category for extending the Add menu, toolbar panels
#   and search operator
# Base class for node categories
#--------------------------------
class CoronaNodeCategory( NodeCategory):
    @classmethod
    def poll(cls, context):
        renderer = context.scene.render.engine
        return context.space_data.tree_type == 'CoronaNodeTree' and renderer == 'CORONA'


helperMaterialName = "Helper Material for Corona Nodes"

def getHelperMaterial():
    helperMaterial = bpy.data.materials.get(helperMaterialName)
    if helperMaterial is None:
        helperMaterial = newHelperMaterial()
    return helperMaterial

def newHelperMaterial():
    material = bpy.data.materials.new(helperMaterialName)
    material.use_nodes = True
    material.use_fake_user = True
    return material


class CurveMapPointCache(bpy.types.PropertyGroup):
    handle_type = bpy.props.StringProperty()
    location = bpy.props.FloatVectorProperty(size = 2)

class CurveMapCache(bpy.types.PropertyGroup):
    extend = bpy.props.StringProperty()
    points = bpy.props.CollectionProperty(type = CurveMapPointCache)
    dirty = bpy.props.BoolProperty(default = True)


# __NODES = []

class CoronaCurveNode2(bpy.types.Node, CoronaNode):
    bl_idname = "CoronaCurveNode2"
    bl_label = "Curve node UI (beta)"
    bl_width_default = 200

    curveMapCache = bpy.props.PointerProperty(type = CurveMapCache)
    num_items = bpy.props.IntProperty(name = 'Number of points', default=10, min=2, soft_max=20)

    def init(self, context):
        # global __NODES
        # __NODES.append(self)
        self.inputs.new( 'CoronaColor', "Input")
        self.inputs.new( 'CoronaLinked', 'UV Map')
        self.outputs.new("NodeSocketShader", "Curve")
        self.createCurveNode()

    def draw_buttons(self, context, layout):
        layout.template_curve_mapping(self.curveNode, "mapping", type = "NONE")
        layout.prop(self, "num_items", "Exported Curve Resolution")

    def get_node_params( self, root, inline = False):
    # def execute(self):
        debug("Curve node execute")
        # load cached curve map if available
        # this happens when the node tree is appended to another file
        if not self.curveMapCache.dirty:
            self.loadCachedCurveMap()
            self.curveMapCache.dirty = True

        mapping = self.mapping
        curve = mapping.curves[3]
        try: curve.evaluate(0.5)
        except: mapping.initialize()
        # return curve.evaluate



        name_pointer = getNodePtr(self)
        xml_mat, xml_mat_def = getMap(root, name_pointer, 'Curve')

        if xml_mat != None:
            addSocket(root, xml_mat, self, "Input", "child", inline)
            for x in range(1, self.num_items+2):
                point = SubElement(xml_mat, 'point')
                pos = (x - 1) / self.num_items
                point.set('position', '%.6f' % pos)
                point.text = '%.6f' % curve.evaluate(pos)
                # xml_mat.append(self.inputs["Point %d" % x].get_socket_value(self, root))
            addChild(root, xml_mat, self, "UV Map", True)
            if not inline: root.append(xml_mat_def)

        if inline:
            return xml_mat
        else:
            return getMapReference(name_pointer)

    def createCurveNode(self):
        material = getHelperMaterial()
        debug("helper material")
        node = material.node_tree.nodes.new("ShaderNodeRGBCurve")
        node.name = self.name
        try: del self["curveNodeName"]
        except: pass
        mapping = self.mapping
        mapping.use_clip = True
        mapping.clip_min_y = -0.5
        mapping.clip_max_y = 1.5
        self.resetEndPoints()
        return node

    def removeCurveNode(self):

        debug("remove curve node")
        material = getHelperMaterial()
        tree = material.node_tree
        curveNode = tree.nodes.get(self.name)
        if curveNode is not None:
            tree.nodes.remove(curveNode)

    def resetEndPoints(self):
        debug("reset end points")

        points = self.curve.points
        points[0].location = (0, 0)
        points[-1].location = (1, 1)
        self.mapping.update()

    def duplicate(self, sourceNode):

        debug("duplicate")
        self.createCurveNode()
        self.copyOtherCurve(sourceNode.curve)

    def delete(self):
        debug("delete")
        self.removeCurveNode()

    def cacheCurveMap(self):

        debug("cache curve map")
        curve = self.curve
        self.curveMapCache.extend = curve.extend
        self.curveMapCache.points.clear()
        for point in curve.points:
            item = self.curveMapCache.points.add()
            item.handle_type = point.handle_type
            item.location = point.location
        self.curveMapCache.dirty = False

    def loadCachedCurveMap(self):
        self.copyOtherCurve(self.curveMapCache)

    def copyOtherCurve(self, otherCurve):
        curve = self.curve
        curve.extend = otherCurve.extend
        curvePoints = curve.points
        for i, point in enumerate(otherCurve.points):
            if len(curvePoints) == i:
                curvePoints.new(50, 50) # random start position
            curvePoints[i].location = point.location
            curvePoints[i].handle_type = point.handle_type
        self.mapping.update()

    @property
    def curve(self):
        return self.mapping.curves[3]

    @property
    def mapping(self):
        return self.curveNode.mapping

    @property
    def curveNode(self):
        material = getHelperMaterial()
        node = material.node_tree.nodes.get(getattr(self, '["curveNodeName"]', self.name))
        if node is None: node = self.createCurveNode()
        return node



#--------------------------------
# Corona node categories
# identifier, label, items list
#--------------------------------
corona_node_categories = [
    CoronaNodeCategory("SHADERUTILS", "Shader Utils/Maps", items = [
        NodeItem( "CoronaAONode"),
        NodeItem( "CoronaChannelNode"),
        NodeItem( "CoronaCurveNode"),
        NodeItem( "CoronaCurveNode2"),
        NodeItem( "CoronaDataNode"),
        NodeItem( "CoronaFalloffNode"),
        NodeItem( "CoronaFresnelNode"),
        NodeItem( "CoronaFrontBackNode"),
        NodeItem( "CoronaInterpolationNode"),
        NodeItem( "CoronaMixNode"),
        NodeItem( "CoronaNormalNode"),
        NodeItem( "CoronaRaySwitchNode"),
        NodeItem( "CoronaRoundEdgesNode"),
        NodeItem( "CoronaSelectNode"),
        NodeItem( "CoronaToneMapNode"),
        NodeItem( "CoronaWireframeNode"),
        NodeItem( "CoronaObjectListNode")
        ]),
    CoronaNodeCategory("TEXTURE", "Textures/Maps", items = [
        NodeItem( "CoronaCheckerNode"),
        NodeItem( "CoronaGradientNode"),
        NodeItem( "CoronaNoiseNode"),
        NodeItem( "CoronaSolidNode"),
        NodeItem( "CoronaTexNode"),
        NodeItem( "CoronaUVWNode")
        ]),
    CoronaNodeCategory("OUTPUT", "Output/Materials", items = [
        NodeItem( "CoronaOutputNode"),
        NodeItem( "CoronaRaySwitchOutputNode"),
        NodeItem( "CoronaLayeredOutputNode"),
        NodeItem( "CoronaMtlNode"),
        NodeItem( "CoronaLightMtlNode"),
        NodeItem( "CoronaVolumeMtlNode"),
        NodeItem( "CoronaShadowcatcherOutputNode")])]

@persistent
def scene_loaded(dummy):
    # Load images as icons
    icon16 = bpy.data.images.get('CORONA16')
    icon32 = bpy.data.images.get('CORONA32')
    if icon16 is None:
        img = bpy.data.images.load(os.path.join( os.path.join( addon_dir, 'icons'), 'corona16.png'))
        img.name = 'CORONA16'
        img.use_alpha = True
        img.user_clear()
    # remove scene_update handler
    elif "corona16" not in icon16.keys():
        icon16["corona16"] = True
        for f in bpy.app.handlers.scene_update_pre:
            if f.__name__ == "scene_loaded":
                bpy.app.handlers.scene_update_pre.remove(f)
    if icon32 is None:
        img = bpy.data.images.load(os.path.join( os.path.join( addon_dir, 'icons'), 'corona32.png'))
        img.name = 'CORONA32'
        img.use_alpha = True
        img.user_clear() # Won't get saved into .blend files
    # remove scene_update handler
    elif "corona32" not in icon32.keys():
        icon32["corona32"] = True
        for f in bpy.app.handlers.scene_update_pre:
            if f.__name__ == "scene_loaded":
                bpy.app.handlers.scene_update_pre.remove(f)


# @persistent
# def savePre(scene):
#     # global __NODES
#     # for node in __NODES:
#     #     node.cacheCurveMap()
#     pass

def register():
    bpy.app.handlers.load_post.append(scene_loaded)
    bpy.app.handlers.scene_update_pre.append(scene_loaded)
    # bpy.app.handlers.save_pre.append(savePre)
    nodeitems_utils.register_node_categories("CORONA_NODES", corona_node_categories)
    bpy.utils.register_class( CoronaNodeTree)
    bpy.utils.register_class( CoronaTexNode)
    bpy.utils.register_class( CoronaAONode)
    bpy.utils.register_class( CoronaMtlNode)
    bpy.utils.register_class( CoronaLightMtlNode)
    bpy.utils.register_class( CoronaVolumeMtlNode)
    bpy.utils.register_class( CoronaMixNode)
    bpy.utils.register_class( CoronaRaySwitchNode)
    bpy.utils.register_class( CoronaSolidNode)
    bpy.utils.register_class( CoronaCheckerNode)
    bpy.utils.register_class( CoronaNoiseNode)
    bpy.utils.register_class( CoronaUVWNode)
    bpy.utils.register_class( CoronaShadowcatcherOutputNode)

def unregister():
    nodeitems_utils.unregister_node_categories("CORONA_NODES")
    bpy.app.handlers.load_post.remove(scene_loaded)
    # bpy.app.handlers.scene_update_pre.remove(scene_loaded)
    # bpy.app.handlers.save_pre.remove(savePre)
    bpy.utils.unregister_class( CoronaNodeTree)
    bpy.utils.unregister_class( CoronaTexNode)
    bpy.utils.unregister_class( CoronaAONode)
    bpy.utils.unregister_class( CoronaMtlNode)
    bpy.utils.unregister_class( CoronaLightMtlNode)
    bpy.utils.unregister_class( CoronaVolumeMtlNode)
    bpy.utils.unregister_class( CoronaMixNode)
    bpy.utils.unregister_class( CoronaRaySwitchNode)
    bpy.utils.unregister_class( CoronaSolidNode)
    bpy.utils.unregister_class( CoronaCheckerNode)
    bpy.utils.unregister_class( CoronaNoiseNode)
    bpy.utils.unregister_class( CoronaUVWNode)
    bpy.utils.unregister_class( CoronaShadowcatcherOutputNode)


